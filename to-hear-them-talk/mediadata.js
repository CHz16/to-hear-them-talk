// mediadata.js
// Image & sound data

let imageFilenames = ["lecture0.png", "lecture1.png", "lecture2.png", "lecture-drained.png", "machine0.png", "machine1.png", "machine2.png", "machine3.png", "machine-drained.png", "playground0.png", "playground1.png", "playground2.png", "playground3.png", "playground-drained.png", "research0.png", "research1.png", "research2.png", "research-drained.png"];

// We'll just make the keys by stripping off the extensions, so I don't have to maintain two arrays
// Maybe something to just put into ImagePreloader
let imageKeys = [];
for (let i = 0; i < imageFilenames.length; i++) {
    imageKeys.push(imageFilenames[i].slice(0, imageFilenames[i].lastIndexOf(".")));
}


let sounds = [{key: "lecture", urls: ["lecture.ogg", "lecture.mp3"]}, {key: "machine", urls: ["machine.ogg", "machine.mp3"]}, {key: "playground", urls: ["playground.ogg", "playground.mp3"]}, {key: "research", urls: ["research.ogg", "research.mp3"]}, {key: "shutdown", urls: ["shutdown.ogg", "shutdown.mp3"]}, {key: "startup", urls: ["startup.ogg", "startup.mp3"]}, {key: "whoosh", urls: ["whoosh.ogg", "whoosh.mp3"]}];
