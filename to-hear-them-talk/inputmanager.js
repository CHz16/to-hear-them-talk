// inputmanager.js
// Object to handle keyboard input

function InputManager(bindings, keyTarget) {
    this.normalizeKeyName = function(keyName, keyCode) {
        if (keyName == "Unidentified" || keyName == "OS" || keyName == "Dead" || keyName == "RomanCharacters") {
            // Disambiguate names that can be mapped to multiple keys: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values
            // Unidentified: "The user agent wasn't able to map the event's virtual keycode to a specific key value." Older versions of web browsers throwed this around for special keys a bunch.
            // OS: may be [Windows], [Super], or [Hyper] in older versions of Firefox or IE
            // Dead: Modifier keys like accents and mode switches. Apparently elements that aren't text fields, like my canvas, can't receive composition events, so I don't think there's actually a way for me to identify which dead keys are pressed.
            // RomanCharacters: Firefox version 36 and earlier would return this for both [Eisu] and [Romaji]
            return "\u2049\uFE0F (" + keyCode + ")";
        } else if (keyName == "Decimal") {
            // Old browsers would return this for keypad .
            return ".";
        } else if (keyName == "Multiply") {
            // Old browsers would return this for keypad *
            return "*";
        } else if (keyName == "Add") {
            // Old browsers would return this for keypad +
            return "+";
        } else if (keyName == "Divide") {
            // Old browsers would return this for keypad /
            return "/";
        } else if (keyName == "Subtract") {
            // Old browsers would return this for keypad -
            return "-";
        } else {
            return keyName.toLowerCase();
        }
    }

    this.informDelegates = function(delegates, method, args = []) {
        // Copy in case the delegate adds or removes a delegate during the callback
        let delegatesCopy = delegates.slice();
        for (delegate of delegatesCopy) {
            if (typeof delegate[method] == "function") {
                delegate[method](...args);
            }
        }
    }

    this.handleKeyDown = function(event) {
        // This isn't a "key" but rather a notification that's sent as a keyboard event.
        // I have literally no idea if this can actually be sent to keydown but just in case it is we'll let it go on
        if (event.key == "RcLowBattery") {
            return;
        }

        //console.log(event.key + ": " + keyboardTranslator.niceNameOfKey(this.normalizeKeyName(event.key, event.keyCode)));
        this.informDelegates(this.hoseDrinkers, "rawKeyDown", [event]);

        let keyName = this.normalizeKeyName(event.key, event.keyCode);
        if (this.rebindTarget !== undefined) {
            event.preventDefault();

            // Escape cancels rebind
            if (event.keyCode == 27) {
                this.rebindTarget = undefined;
                this.informDelegates(this.listeners, "rebindCanceled");
                return;
            }

            // key A: currentBindings[this.rebindTarget]
            // action A: this.rebindTarget
            // key B: keyName
            // action B: this.keyBindings[keyName]

            // Currently, key A has been assigned action A, and the user has pressed key B
            // First, check if key A = key B, and if so, just bail out
            let currentBindings = this.getKeyBindings();
            if (currentBindings[this.rebindTarget] == keyName) {
                this.rebindTarget = undefined;
                this.informDelegates(this.listeners, "rebindCanceled");
                return;
            }

            // If key B already has an action B, then we'll swap actions with it, which means assigning action B to key A
            // Otherwise, we unset the action from key A
            if (this.keyBindings[keyName] !== undefined) {
                this.keyBindings[currentBindings[this.rebindTarget]] = this.keyBindings[keyName];
            } else {
                this.keyBindings[currentBindings[this.rebindTarget]] = undefined;
            }

            // Now set action A to key B
            this.keyBindings[keyName] = this.rebindTarget;

            // Announce that we rebound
            // Regenerate the currentBindings dictionary to avoid having to swap and clear undefineds
            currentBindings = this.getKeyBindings();
            this.informDelegates(this.listeners, "keyRebound", [currentBindings]);

            // We don't need to swap the values keyDown because it's indexed by Key
            this.rebindTarget = undefined;
            return;
        }

        let key = (event.keyCode == 27) ? Key.escape : this.keyBindings[keyName];
        if (key !== undefined) {
            event.preventDefault();
            this.keyDown[key] = true;
            this.informDelegates(this.listeners, "keyDown", [key]);
        }
    }

    this.handleKeyUp = function(event) {
        // This isn't a "key" but rather a notification that's sent as a keyboard event.
        // I have literally no idea if this can actually be sent to keydown but just in case it is we'll let it go on
        if (event.key == "RcLowBattery") {
            return;
        }

        this.informDelegates(this.hoseDrinkers, "rawKeyUp", [event]);

        let key = this.keyBindings[this.normalizeKeyName(event.key, event.keyCode)];
        if (key !== undefined) {
            event.preventDefault();
            this.keyDown[key] = false;
            this.informDelegates(this.listeners, "keyUp", [key]);
        }
    }

    this.handleBlur = function(event) {
        for (key in this.keyDown) {
            if (this.keyDown[key]) {
                this.keyDown[key] = false;
                this.informDelegates(this.listeners, "keyUp", [key]);
            }
        }
    }

    this.rebindTarget = undefined;

    // Set up the internal data structures
    this.keyBindings = {};
    this.keyDown = [];
    this.listeners = [];
    this.hoseDrinkers = [];

    // Read in the arguments
    for (key in bindings) {
        this.keyBindings[this.normalizeKeyName(bindings[key])] = key;
        this.keyDown[key] = false;
    }

    // Listen for key events
    keyTarget.addEventListener("keydown", this.handleKeyDown.bind(this));
    keyTarget.addEventListener("keyup", this.handleKeyUp.bind(this));
    keyTarget.addEventListener("blur", this.handleBlur.bind(this));
}

InputManager.prototype.registerListener = function(listener) {
    if (!this.listeners.includes(listener)) {
        this.listeners.push(listener);
    }
}

InputManager.prototype.unregisterListener = function(listener) {
    let i = this.listeners.indexOf(listener);
    if (i != -1) {
        this.listeners.splice(i, 1);
    }
}

InputManager.prototype.turnOnTheHose = function(listener) {
    if (!this.hoseDrinkers.includes(listener)) {
        this.hoseDrinkers.push(listener);
    }
}

InputManager.prototype.turnOffTheHose = function(listener) {
    let i = this.hoseDrinkers.indexOf(listener);
    if (i != -1) {
        this.hoseDrinkers.splice(i, 1);
    }
}

InputManager.prototype.rebind = function(key) {
    this.rebindTarget = key;
}

InputManager.prototype.keyIsDown = function(key) {
    let b = this.keyDown[key];
    return (b !== undefined) ? b : false;
}

InputManager.prototype.getKeyBindings = function() {
    let bindings = {};
    for (keyName in this.keyBindings) {
        if (this.keyBindings[keyName] !== undefined) {
            bindings[this.keyBindings[keyName]] = keyName;
        }
    }
    return bindings;
}

InputManager.prototype.forceSetKeyBindings = function(bindings) {
    this.keyBindings = {};
    for (key in bindings) {
        this.keyBindings[bindings[key]] = key;
        this.keyDown[key] = false;
    }

    currentBindings = this.getKeyBindings();
    this.informDelegates(this.listeners, "keyRebound", [currentBindings]);
}

InputManager.prototype.nameForKey = function(key) {
    for (keyName in this.keyBindings) {
        if (this.keyBindings[keyName] == key) {
            return keyName;
        }
    }
    return undefined;
}
