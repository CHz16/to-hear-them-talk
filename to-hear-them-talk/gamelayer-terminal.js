﻿// gamelayer-terminal.js
// GameLayer extension methods for updating/drawing the terminal, because there's a lot of this

let GAME_TERMINAL_STATUS_DURATION = 2000, GAME_TERMINAL_STATUS_FADE_DURATION = 250; // milliseconds
let GAME_TERMINAL_STATUS_PHASE_DURATION = GAME_TERMINAL_STATUS_DURATION + 2 * GAME_TERMINAL_STATUS_FADE_DURATION;
let GAME_TERMINAL_STATUS_FULL_DURATION = 4 * GAME_TERMINAL_STATUS_PHASE_DURATION;

GameLayer.prototype.updateTerminal = function() {
    // If we're updating for the first time after opening this or we've switched status phase, then regenerated the random data
    let activeTerminalCircle = Math.floor(this.t / GAME_TERMINAL_STATUS_PHASE_DURATION) % 4;
    if (this.activeTerminalCircle != activeTerminalCircle) {
        this.activeTerminalCircle = activeTerminalCircle;
        this.terminalData = [];
        for (let i = 0; i < [100, 120, 95, 80][activeTerminalCircle]; i++) {
            this.terminalData.push(Math.random());
        }

        if (activeTerminalCircle == 1) {
            this.startBox = Math.floor(Math.random() * 60);
        }
    }
}

GameLayer.prototype.drawTerminal = function(context) {
    let phaseT = this.t % GAME_TERMINAL_STATUS_PHASE_DURATION;
    let fadePosition = 0;
    if (phaseT < GAME_TERMINAL_STATUS_FADE_DURATION) {
        fadePosition = 1 - phaseT / GAME_TERMINAL_STATUS_FADE_DURATION;
    } else if (phaseT > GAME_TERMINAL_STATUS_DURATION + GAME_TERMINAL_STATUS_FADE_DURATION) {
        fadePosition = 1 - (GAME_TERMINAL_STATUS_PHASE_DURATION - phaseT) / GAME_TERMINAL_STATUS_FADE_DURATION;
    }

    // Frame and backgrounds
    context.strokeStyle = "black";
    context.lineWidth = 3;
    context.save();
    context.shadowColor = "black";
    context.shadowBlur = 10;
    context.strokeRect(50, 50, gameWidth - 100, gameHeight - 100);
    context.restore();
    context.fillStyle = "white";
    context.fillRect(50, 50, gameWidth - 100, gameHeight - 100);
    context.fillStyle = "black";
    context.fillRect(70, 70, gameWidth - 140, gameHeight - 140);
    context.fillStyle = "red";
    context.beginPath();
    context.arc(737.5, 537.5, 5, 0, 2 * Math.PI);
    context.fill();

    // Status circles
    context.strokeStyle = "#30FF30";
    context.strokeRect(80, 80, gameWidth - 160, 250);
    for (let i = 0; i < 4; i++) {
        if (i == this.activeTerminalCircle) {
            context.fillStyle = GAME_ROOM_COLOR_SOURCES[i].lowColor(1.5 * fadePosition);
        } else {
            context.fillStyle = GAME_ROOM_COLOR_SOURCES[i].lowColor(1.5);
        }
        context.beginPath();
        context.arc(145 + i * 170, 145, 60, 0, 2 * Math.PI);
        context.fill();

        let character = ["፩", "፪", "፫", "፬"][i];
        context.font = "40px serif";
        context.fillStyle = "black";
        let metrics = context.measureText(character);
        drawText(context, character, 145 + i * 170 - metrics.width / 2, 126, 40);
    }

    // Draw the data whatsit
    context.save();
    context.globalAlpha = 1 - fadePosition;
    switch (this.activeTerminalCircle) {
        case 0:
            this.drawTerminalLineGraph(context, phaseT);
            break;
        case 1:
            this.drawTerminalBoxes(context, phaseT);
            break;
        case 2:
            this.drawTerminalBarGraph(context, phaseT);
            break;
        case 3:
            this.drawTerminalConstellations(context, phaseT);
            break;
    }
    context.restore();

    // Machine orientation
    context.strokeStyle = "#30FF30";
    context.fillStyle = "#30FF30";
    context.lineWidth = 3;
    context.save();
    context.translate(630, 430);
    if (this.gamePhase == GamePhase.resetting) {
        // progress³ so rotation speed increases over time
        let progress = 1 - (this.animationEndTime - this.t) / GAME_ENDGAME_STARTUP_DURATION;
        progress *= progress * progress;
        context.rotate(progress * 6 * Math.PI);
    }
    context.beginPath();
    context.arc(0, 0, 90, 0, 2 * Math.PI);
    context.stroke();
    context.beginPath();
    context.moveTo(-10, -70);
    context.lineTo(-10, 70);
    context.lineTo(-80, 0);
    context.fill();
    context.beginPath();
    context.moveTo(10, -70);
    context.lineTo(10, 70);
    context.lineTo(80, 0);
    context.fill();
    context.restore();

    // Menu text
    context.font = "30px monospace";
    let menuText = ["Waiting for command:", "Begin experiment", "Shut down"];
    for (let i = 0; i < menuText.length; i++) {
        if (i - 1 == this.terminalMenuOption) {
            let optionMetrics = context.measureText(menuText[i]);
            context.fillStyle = "#30FF30";
            context.fillRect(80, 365 + 50 * i, optionMetrics.width, 30);
            context.fillStyle = "black";
        } else {
            context.fillStyle = "#30FF30";
        }
        drawText(context, menuText[i], 80, 365 + 50 * i, 30);
    }
}


let GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH = 15;
GameLayer.prototype.drawTerminalLineGraph = function(context, phaseT) {
    // Axes
    context.strokeStyle = "#30FF30";
    context.beginPath();
    context.moveTo(100, 270);
    context.lineTo(700, 270);
    context.moveTo(120, 225);
    context.lineTo(120, 315);
    context.stroke();

    // Graph
    context.save();
    context.beginPath();
    context.rect(100, 225, 600, 90);
    context.clip();

    let graphStart = -(phaseT / GAME_TERMINAL_STATUS_PHASE_DURATION) * ((this.terminalData.length - 1) * GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH - 600);
    let x = 100 + graphStart % GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH;
    let n = Math.floor(-graphStart / GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH);

    context.lineWidth = 1;
    context.beginPath();
    context.moveTo(x, 225 + 90 * this.terminalData[n]);
    while (x < 700) {
        let newX = x + GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH;
        n += 1;
        context.quadraticCurveTo((x + newX) / 2, 270, newX, 225 + 90 * this.terminalData[n]);
        x += GAME_TERMINAL_LINE_GRAPH_SEGMENT_LENGTH;
    }
    context.stroke();

    context.restore();
}

GameLayer.prototype.drawTerminalBoxes = function(context, phaseT) {
    let startBox = this.startBox + Math.floor(120 * phaseT / GAME_TERMINAL_STATUS_PHASE_DURATION);
    let colorSource = new ColorSource(48, 255, 48);
    for (let n = startBox; n < startBox + 60; n++) {
        let x = n % 20, y = Math.floor((n % 60) / 20);
        context.fillStyle = colorSource.lowColor(this.terminalData[n % 120]);
        context.fillRect(100 + x * 30, 225 + y * 30, 30, 30);
    }
}

GameLayer.prototype.drawTerminalBarGraph = function(context, phaseT) {
    // Axes
    context.strokeStyle = "#30FF30";
    context.beginPath();
    context.moveTo(100, 305);
    context.lineTo(700, 305);
    context.moveTo(110, 225);
    context.lineTo(110, 315);
    context.stroke();

    // Bars
    let barSet = Math.floor(4 * phaseT / GAME_TERMINAL_STATUS_PHASE_DURATION);
    let barProgress = (phaseT - barSet * GAME_TERMINAL_STATUS_PHASE_DURATION / 4) / (GAME_TERMINAL_STATUS_PHASE_DURATION / 4);
    context.fillStyle = "#30FF30";
    for (let i = 0; i < 19; i++) {
        let height = (1 - barProgress) * this.terminalData[barSet * 19 + i] + barProgress * this.terminalData[(barSet + 1) * 19 + i];
        height *= 75;
        context.fillRect(115 + i * 31, 300 - height, 26, height);
    }
}

GameLayer.prototype.drawTerminalConstellations = function(context, phaseT) {
    let pointSet = Math.floor(3 * phaseT / GAME_TERMINAL_STATUS_PHASE_DURATION);
    let pointProgress = (phaseT - pointSet * GAME_TERMINAL_STATUS_PHASE_DURATION / 3) / (GAME_TERMINAL_STATUS_PHASE_DURATION / 3);

    let points = [];
    for (let i = 0; i < 10; i++) {
        let x = (1 - pointProgress) * this.terminalData[pointSet * 20 + i] + pointProgress * this.terminalData[(pointSet + 1) * 20 + i];
        let y = (1 - pointProgress) * this.terminalData[pointSet * 20 + i + 1] + pointProgress * this.terminalData[(pointSet + 1) * 20 + i + 1];
        points.push({x: x * 600 + 100, y: y * 90 + 225});
    }

    context.strokeStyle = "#30FF30";
    context.lineWidth = 1;
    for (let i = 0; i < 10; i++) {
        context.beginPath();
        context.arc(points[i].x, points[i].y, 4, 0, 2 * Math.PI);
        for (let j = 1; j < 3; j++) {
            context.moveTo(points[i].x, points[i].y);
            context.lineTo(points[(i + j) % 10].x, points[(i + j) % 10].y);
        }
        context.stroke();
    }
}
