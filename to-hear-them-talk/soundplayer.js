// soundplayer.js
// Object for simple playback of sound effects.

function SoundPlayer(soundBuffers, context, destination) {
    this.soundBuffers = soundBuffers;
    this.soundChannels = new Object();
    this.context = context;

    this.isPaused = false;
    this.pausedChannels = undefined;

    this.masterGain = this.context.createGain();
    this.masterGain.gain.setValueAtTime(1, 0);
    this.masterGain.connect(destination !== undefined ? destination: this.context.destination);
}

SoundPlayer.prototype.play = function(key, args = {}) {
    let loop = (args.loop !== undefined) ? args.loop : false;
    let offset = (args.offset !== undefined) ? args.offset : 0; // used internally for pause resumption, don't actually pass this in

    // If we're paused, then queue this sound to start immediately on unpause and then bail out
    // Otherwise, start up the sound now
    if (this.isPaused) {
        // This will clobber any other paused channel of this sound
        this.pausedChannels[key] = {loop: loop, offset: offset, x: args.x, y: args.y};
        return;
    }

    // Don't allow multiple copies of a sound to play at once
    // Maybe remove this restriction at some point
    if (key in this.soundChannels) {
        this.stop(key);
    }

    let sourceNode = this.context.createBufferSource(), gainNode = this.context.createGain();
    this.soundChannels[key] = {sourceNode: sourceNode, gainNode: gainNode, startTime: this.context.currentTime - offset, loop: loop};
    sourceNode.buffer = this.soundBuffers[key];
    if (loop) {
        sourceNode.loop = true;
    }
    sourceNode.onended = function() { this.stop(key); }.bind(this);

    if (args.x === undefined) {
        sourceNode.connect(gainNode);
    } else {
        let pannerNode = this.context.createPanner();
        pannerNode.setPosition(args.x, args.y, -50);
        pannerNode.setOrientation(0, 0, 1);
        pannerNode.refDistance = 50;
        pannerNode.maxDistance = 300;
        pannerNode.distanceModel = "linear";
        this.soundChannels[key].pannerNode = pannerNode;
        this.soundChannels[key].x = args.x;
        this.soundChannels[key].y = args.y;
        sourceNode.connect(pannerNode);
        pannerNode.connect(gainNode);
    }
    gainNode.connect(this.masterGain);
    sourceNode.start(0, offset);
}

SoundPlayer.prototype.pause = function() {
    if (this.isPaused) {
        return;
    }

    this.isPaused = true;
    this.pausedChannels = {};
    for (key in this.soundChannels) {
        if (!this.soundChannels[key].dontResume) {
        this.pausedChannels[key] = {
                offset: (this.context.currentTime - this.soundChannels[key].startTime) % this.soundChannels[key].sourceNode.buffer.duration,
                loop: this.soundChannels[key].loop,
                x: this.soundChannels[key].x,
                y: this.soundChannels[key].y
            };
        }
        this.stop(key);
    }
}

SoundPlayer.prototype.unpause = function() {
    if (!this.isPaused) {
        return;
    }

    this.isPaused = false;
    for (key in this.pausedChannels) {
        this.play(key, {offset: this.pausedChannels[key].offset, loop: this.pausedChannels[key].loop, x: this.pausedChannels[key].x, y: this.pausedChannels[key].y});
    }
    this.pausedChannels = undefined;
}

SoundPlayer.prototype.stop = function(key) {
    if (key in this.soundChannels) {
        this.soundChannels[key].sourceNode.onended = undefined;
        this.soundChannels[key].sourceNode.stop();
        this.soundChannels[key].sourceNode.disconnect();
        if (this.soundChannels[key].pannerNode !== undefined) {
            this.soundChannels[key].pannerNode.disconnect();
        }
        delete this.soundChannels[key];
    }
}

SoundPlayer.prototype.stopAllSounds = function() {
    for (key in this.soundChannels) {
        this.stop(key);
    }
    if (this.pausedChannels !== undefined) {
        this.pausedChannels = {};
    }
}

SoundPlayer.prototype.setVolume = function(volume) {
    this.masterGain.gain.setValueAtTime(volume, 0);
}

SoundPlayer.prototype.fadeOut = function(key) {
    if (this.paused) {
        delete this.pausedChannels[key];
    } else {
        this.soundChannels[key].dontResume = true;
        this.soundChannels[key].gainNode.gain.setTargetAtTime(0, 0, 0.3);
    }
}

SoundPlayer.prototype.setPositionOfListener = function(x, y) {
    if (this.context.positionX !== undefined) {
        this.context.listener.positionX.value = x;
        this.context.listener.positionY.value = y;
    } else {
        this.context.listener.setPosition(x, y, 0);
    }
}
