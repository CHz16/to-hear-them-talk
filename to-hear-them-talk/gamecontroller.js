// gamecontroller.js
// To hear them talk main game code.

function GameController() {
    this.showingMenu = true;

    this.gameLayer = new GameLayer();
    this.gameLayer.initialize();
    this.menuLayer = new MenuLayer();
    this.menuLayer.resetMenus();
    inputManager.registerListener(this.menuLayer);
}

// Game loop hooks
GameController.prototype.draw = function(timestamp, dt) {
    if (!this.showingMenu) {
        this.gameLayer.update(timestamp, dt);
    } else {
        this.menuLayer.update(timestamp, dt);
    }
    this.gameLayer.draw(canvasContext);
    if (this.showingMenu) {
        canvasContext.fillStyle = "rgba(0, 0, 0, 0.2)";
        canvasContext.fillRect(0, 0, gameWidth, gameHeight);
        this.menuLayer.draw(canvasContext);
    }
}

GameController.prototype.pause = function(timestamp) {
    (this.showingMenu ? this.menuLayer : this.gameLayer).pause(timestamp);
}

GameController.prototype.unpause = function(pauseTime) {
    (this.showingMenu ? this.menuLayer : this.gameLayer).unpause(pauseTime);
}

// InputManager hooks
GameController.prototype.keyDown = function(key) {
    if (key == Key.escape && !this.showingMenu) {
        this.showingMenu = true;
        this.gameLayer.pause();
        inputManager.unregisterListener(this.gameLayer);
        inputManager.registerListener(this.menuLayer);
        this.menuLayer.unpause();
    }
}

GameController.prototype.keyUp = function(key) {

}

GameController.prototype.keyRebound = function(currentBindings) {

}

GameController.prototype.rebindCanceled = function() {

}

// Actions from the menu layer
GameController.prototype.start = function(shouldRestart) {
    if (shouldRestart) {
        this.gameLayer.initialize();
    }

    this.showingMenu = false;
    this.menuLayer.pause();
    inputManager.unregisterListener(this.menuLayer);
    inputManager.registerListener(this.gameLayer);
    this.gameLayer.unpause();
}
