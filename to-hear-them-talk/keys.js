// keys.js
// Key definitions

// There may be some == comparison with undefined in the input manager so we'll start these indexes at 1
let Key = {up: 1, down: 2, left: 3, right: 4, action: 5, cancel: 6, escape: 999};
let defaultKeyBindings = {};
defaultKeyBindings[Key.up] = "arrowup";
defaultKeyBindings[Key.down] = "arrowdown";
defaultKeyBindings[Key.left] = "arrowleft";
defaultKeyBindings[Key.right] = "arrowright";
defaultKeyBindings[Key.action] = "z";
defaultKeyBindings[Key.cancel] = "x";
