// game.js
// To hear them talk game loading, setup, and base layer code.


//
// GAME PARAMETERS
//

let gameWidth = 800, gameHeight = 600;


//
// GLOBALS
//

let loadingError, mediaLoaded = 0, mediaCount = imageKeys.length + sounds.length;

let audioContext;
let soundBuffers, masterVolumeNode;

let imagePreloader;
let canvas, canvasContext;

let isPaused = false, pauseTime;
let lastUpdateTime;

let defaults = new JSUserDefaults("tohearthemtalk_", {
    keyBindings: defaultKeyBindings,
    volume: 100,
    tankControls: true
});
let keyboardTranslator = new KeyboardTranslator(), inputManager, gameController;


//
// INITIALIZATION
//

window.addEventListener("load", init);
function init() {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    canvas = canvasThings.canvas;
    canvasContext = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.body.replaceChild(canvas, document.getElementById("canvas"));
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";

    canvas.onmouseup = function(evt) {
        canvas.onmouseup = undefined;
        init2();
    }
    drawMessage("(click to play)", true);
}

function init2() {
    // Set up the audio context immediately after we've gotten user input, to appease Chrome & Safari
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    audioContext.resume(); // Safari 12 autoplay policy pls
    masterVolumeNode = audioContext.createGain();
    updateVolume();
    masterVolumeNode.connect(audioContext.destination);

    drawMessage("loaded " + 0 + "/" + mediaCount, true);

    // Load the images
    let imageLoadCallback = function(preloader, imagesHandled, index) {
        if (loadingError !== undefined) {
            return true;
        }
        mediaLoaded += 1;
        drawMessage("loaded " + mediaLoaded + "/" + mediaCount, true);
        if (mediaLoaded == mediaCount) {
            init3();
        }
    };
    let imageErrorCallback = function(preloader, imagesHandled, index) {
        loadingError = "image: " + preloader.keys[index];
        drawMessage("error loading " + loadingError, true);
        return true;
    }
    imagePreloader = new ImagePreloader(imageFilenames, {base: "images/", keys: imageKeys, loadCallback: imageLoadCallback, errorCallback: imageErrorCallback});

    // Load the sounds
    let soundLoadCallback = function(preloader, soundsHandled, index) {
        if (loadingError !== undefined) {
            return true;
        }
        mediaLoaded += 1;
        drawMessage("loaded " + mediaLoaded + "/" + mediaCount, true);
        if (soundsHandled == preloader.sounds.length) {
            soundBuffers = preloader.soundBuffers;
        }
        if (mediaLoaded == mediaCount) {
            init3();
        }
    };
    let soundErrorCallback = function(preloader, soundsHandled, index) {
        loadingError = "sound: " + preloader.sounds[index].key;
        drawMessage("error loading " + loadingError, true);
        return true;
    };
    let soundPreloader = new SoundPreloader(sounds, audioContext, {base: "sounds/", loadCallback: soundLoadCallback, errorCallback: soundErrorCallback});
}

function init3() {
    // Set up canvas events for focusing
    canvas.tabIndex = 0;
    canvas.addEventListener("focus", function() {
        if (isPaused) {
            isPaused = false;
            gameController.unpause(pauseTime);
            lastUpdateTime = undefined;
            pauseTime = undefined;
            window.requestAnimationFrame(draw);
        }
    });
    canvas.addEventListener("blur", function(event) { canvas.blur(); });
    if (document.hasFocus()) {
        canvas.focus();
    }

    // Start listening for user input too
    inputManager = new InputManager(defaults.get("keyBindings"), canvas);
    gameController = new GameController();
    inputManager.registerListener(gameController);

    // Kick off the draw loop
    window.requestAnimationFrame(draw);
}


//
// AUDIO
//

function updateVolume() {
    masterVolumeNode.gain.setValueAtTime(defaults.get("volume") / 100, 0);
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

// Draws text with an upper left of (x, y)
// Could also do this with context.textBaseline = "top", but that renders inconsistently between FF and Safari/Chrome
function drawText(context, text, x, y, fontSize) {
    context.fillText(text, Math.round(x), Math.round(y + 3 * fontSize / 4));
}

function drawMessage(text, clear = false) {
    // Clear the canvas
    if (clear) {
        canvasContext.fillStyle = "white";
        canvasContext.fillRect(0, 0, gameWidth, gameHeight);
    }

    canvasContext.font = "30px sans-serif";
    let metrics = canvasContext.measureText(text);

    let x = Math.round((gameWidth - metrics.width) / 2)
    let y = 300

    canvasContext.fillStyle = "white";
    canvasContext.fillRect(x - 5, y - 5, metrics.width + 10, 40);
    canvasContext.strokeStyle = "black";
    canvasContext.lineWidth = 2;
    canvasContext.strokeRect(x - 5, y - 5, metrics.width + 10, 40);

    canvasContext.fillStyle = "black";
    drawText(canvasContext, text, x, y, 30);
}

function draw(timestamp) {
    if (lastUpdateTime === undefined) {
        lastUpdateTime = timestamp;
    }
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;

    // Clear the canvas
    canvasContext.fillStyle = "white";
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);

    // Draw
    gameController.draw(timestamp, dt);

    if (document.activeElement != canvas) {
        // If the canvas has lost focus, then pause the game.
        isPaused = true;
        pauseTime = timestamp;
        gameController.pause(pauseTime);

        // Darken the last-drawn frame and draw a message
        canvasContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        canvasContext.fillRect(0, 0, gameWidth, gameHeight);
        drawMessage("(click to resume)");
    } else {
        // Otherwise if we still have focus, then request another draw
        window.requestAnimationFrame(draw);
    }
}
