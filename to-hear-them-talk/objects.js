// objects.js
// Object definitions for the game map.

let GAME_DRAIN_ANIMATION_SPREAD_DISTANCE = 1000; // pixels

function Hallway() {
    this.x = 0, this.y = 380;
    this.width = 0, this.height = 200;

    this.colorSource = undefined, this.emptyProgress = undefined, this.doorLeft = undefined;
}

Hallway.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    context.fillStyle = "white";
    context.fillRect(0, 0, this.width, this.height);

    if (this.emptyProgress !== undefined) {
        let doorWidth = 40, doorHeight = 70, doorY = 30;

        let fillWidth = doorWidth + 2 * GAME_DRAIN_ANIMATION_SPREAD_DISTANCE * this.emptyProgress;
        let fillLeft = this.doorLeft + (doorWidth - fillWidth) / 2;
        let fillHeight = Math.min(100, GAME_DRAIN_ANIMATION_SPREAD_DISTANCE * this.emptyProgress);

        // Upper segment - gradient across door & wall
        context.fillStyle = this.colorSource.gradient(context, 0, doorY, 0, doorY + doorHeight, this.emptyProgress);
        context.fillRect(fillLeft, doorY, fillWidth, doorHeight);

        // Lower segment - floor
        context.fillStyle = this.colorSource.colorAt(this.emptyProgress);
        context.fillRect(fillLeft, doorY + doorHeight, fillWidth, fillHeight);
    }

    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(this.width, 0);
    context.moveTo(0, 20);
    context.lineTo(this.width, 20);
    context.moveTo(0, 100);
    context.lineTo(this.width, 100);
    context.moveTo(0, 200);
    context.lineTo(this.width, 200);
    context.stroke();

    context.restore();
}


function Endcap() {
    this.x = 0, this.y = 380;
    this.width = 50, this.height = 200;
}

Endcap.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    // Calculations are +1 here in a few places to avoid seams when joining with Hallway

    // Fill color
    context.fillStyle = "white";
    context.beginPath();
    context.moveTo(this.width + 1, 0);
    context.lineTo(this.width + 1 - 10, 0);
    context.lineTo(0, 120);
    context.lineTo(0, 200);
    context.lineTo(this.width + 1, 200);
    context.closePath();
    context.fill();

    // Lines
    context.lineJoin = "bevel";
    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.beginPath();
    context.moveTo(this.width + 1, 0);
    context.lineTo(this.width + 1 - 10, 0);
    context.lineTo(0, 120);
    context.lineTo(0, 200);
    context.lineTo(this.width + 1, 200);
    context.moveTo(0, 120);
    context.lineTo(20, 120);
    context.lineTo(this.width + 1, 20);
    context.lineTo(this.width + 1, 100);
    context.lineTo(20, 200);
    context.lineTo(20, 120);
    context.stroke();

    context.restore();
}


// Honestly faster just top copy-paste this than mess with mirror transforms lmao
function RightEndcap() {
    this.x = 0, this.y = 380;
    this.width = 50, this.height = 200;
}

RightEndcap.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    // Calculations are -1 here in a few places to avoid seams when joining with Hallway

    // Fill color
    context.fillStyle = "white";
    context.beginPath();
    context.moveTo(-1, 0);
    context.lineTo(-1 + 10, 0);
    context.lineTo(this.width, 120);
    context.lineTo(this.width, 200);
    context.lineTo(-1, 200);
    context.closePath();
    context.fill();

    // Lines
    context.lineJoin = "bevel";
    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.beginPath();
    context.moveTo(-1, 0);
    context.lineTo(-1 + 10, 0);
    context.lineTo(this.width, 120);
    context.lineTo(this.width, 200);
    context.lineTo(-1, 200);
    context.moveTo(this.width, 120);
    context.lineTo(this.width - 20, 120);
    context.lineTo(-1, 20);
    context.lineTo(-1, 100);
    context.lineTo(this.width - 20, 200);
    context.lineTo(this.width - 20, 120);
    context.stroke();

    context.restore();
}


function Player() {
    this.x = 0, this.y = 0;
    this.width = 40, this.height = 40;
    this.heading = 0;
}

Player.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x + this.width / 2, y + this.height / 2);
    context.rotate(-this.heading);

    // Front arrow
    context.lineWidth = 2;
    context.strokeStyle = "black";
    context.fillStyle = "black";
    context.beginPath();
    context.moveTo(5, -this.height / 2 + 2);
    context.lineTo(5, this.height / 2 - 2);
    context.lineTo(this.width / 2, 0);
    context.closePath();
    context.stroke();
    context.fill();

    // Back arrow
    context.beginPath();
    context.moveTo(-5, -this.height / 2 + 2);
    context.lineTo(-5, this.height / 2 - 2);
    context.lineTo(-this.width / 2, 0);
    context.closePath();
    context.stroke();

    context.restore();
}


function Plaque(color, message) {
    this.x = 0, this.y = 430;
    this.width = 50, this.height = 20;

    this.color = color;
    this.isColored = false;
    this.message = message;

    this.interactionHeight = 50;
    this.interactive = true;
    this.action = "readPlaque";
}

Plaque.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.strokeRect(0, 0, this.width, this.height);

    if (this.isColored) {
        context.strokeStyle = this.color;
        context.lineWidth = 1;
        context.beginPath();
        context.moveTo(5, this.height / 3);
        context.lineTo(this.width - 5, this.height / 3);
        context.moveTo(5, 2 * this.height / 3);
        context.lineTo(this.width - 5, 2 * this.height / 3);
        context.stroke();
    }

    context.restore();
}


function LeftTerminal() {
    this.x = 0, this.y = 440;
    this.width = 21, this.height = 100;

    this.interactive = true;
    this.action = "useTerminal";
}

LeftTerminal.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    // Frame
    context.lineJoin = "bevel";
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.beginPath();
    context.moveTo(this.width, 0);
    context.lineTo(0, 65);
    context.lineTo(0, this.height);
    context.lineTo(this.width, 35);
    context.closePath();
    context.stroke();

    // Screen black
    context.fillStyle = "black";
    context.beginPath();
    context.moveTo(this.width - 2, 12);
    context.lineTo(2, 65);
    context.lineTo(2, this.height - 12);
    context.lineTo(this.width - 2, 35);
    context.closePath();
    context.fill();

    // Screen text
    let frame = Math.floor(t / 500) % 4;
    for (let i = 0; i < 4; i++) {
        context.fillStyle = (i == frame) ? GAME_ROOM_COLOR_SOURCES[i].color : GAME_ROOM_COLOR_SOURCES[i].lowColor(1);
        context.beginPath();
        context.arc(6 + i * 3, 64 - i * 10, 2.5, 0, 2 * Math.PI);
        context.fill();
    }

    context.restore();
}


// Honestly faster just top copy-paste this than mess with mirror transforms lmao
function RightTerminal() {
    this.x = 0, this.y = 440;
    this.width = 21, this.height = 100;

    this.interactive = true;
    this.action = "useTerminal";
}

RightTerminal.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    // Frame
    context.lineJoin = "bevel";
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(this.width, 65);
    context.lineTo(this.width, this.height);
    context.lineTo(0, 35);
    context.closePath();
    context.stroke();

    // Screen black
    context.fillStyle = "black";
    context.beginPath();
    context.moveTo(2, 12);
    context.lineTo(this.width - 2, 65);
    context.lineTo(this.width - 2, this.height - 12);
    context.lineTo(2, 35);
    context.closePath();
    context.fill();

    // Screen text
    let frame = Math.floor(t / 500) % 4;
    for (let i = 0; i < 4; i++) {
        context.fillStyle = (i == frame) ? GAME_ROOM_COLOR_SOURCES[i].color : GAME_ROOM_COLOR_SOURCES[i].lowColor(1);
        context.beginPath();
        context.arc(6 + i * 3, 34 + i * 10, 2.5, 0, 2 * Math.PI);
        context.fill();
    }

    context.restore();
}


function Door(id) {
    this.x = 0, this.y = 410;
    this.width = 40, this.height = 70;

    this.id = id;
    this.open = false;

    this.interactive = true;
    this.action = "openDoor";
}

Door.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.strokeRect(0, 0, this.width, this.height);
    if (!this.open) {
        context.beginPath();
        context.arc(30, 35, 5, 0, 2 * Math.PI);
        context.stroke();
    }

    context.restore();
}


function Room(colorSource, name, frames) {
    this.x = 0, this.y = 50;
    this.width = 550, this.height = 330;

    this.emptyProgress = 0;
    this.colorSource = colorSource;
    this.color = this.colorSource.color;
    this.name = name;
    this.frames = frames;
}

Room.prototype.draw = function(context, x, y, t) {
    context.save();
    context.translate(x, y);

    // Fill the room & walls with white first
    canvas.fillStyle = "white";
    context.beginPath();
    context.moveTo(0, this.height);
    context.lineTo(35, 0);
    context.lineTo(this.width - 35, 0);
    context.lineTo(this.width, this.height);
    context.closePath();
    context.fill();

    // Lines
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.beginPath();
    context.moveTo(0, this.height);
    context.lineTo(35, 0);
    context.lineTo(this.width - 35, 0);
    context.lineTo(this.width, this.height);
    context.lineTo(this.width - 15, this.height);
    context.lineTo(this.width - 50, 15);
    context.lineTo(50, 15);
    context.lineTo(15, this.height);
    context.moveTo(50, 15);
    context.lineTo(50, 95);
    context.lineTo(this.width - 50, 95);
    context.lineTo(this.width - 50, 15);
    context.moveTo(50, 95);
    context.lineTo(25, this.height);
    context.moveTo(this.width - 50, 95);
    context.lineTo(this.width - 25, this.height);
    context.stroke();

    // Contents
    let frame = Math.floor(t % (this.frames * 250) / 250);
    if (this.emptyProgress == 0) {
        context.drawImage(imagePreloader.getImage(this.name + frame), 50, 15);
    } else if (this.emptyProgress == 1) {
        context.drawImage(imagePreloader.getImage(this.name + "-drained"), 50, 15);
    } else {
        let cutoff = 313 * this.emptyProgress;
        context.drawImage(imagePreloader.getImage(this.name + frame), 0, cutoff, 448, 313 - cutoff, 50, 15 + cutoff, 448, 313 - cutoff);
        context.drawImage(imagePreloader.getImage(this.name + "-drained"), 0, 0, 448, cutoff, 50, 15, 448, cutoff);
    }

    // Wall and floor colors now
    // Done on top of everything else with a multiply composite operation so we can color on top of the lines
    if (this.emptyProgress < 1) {
        context.save();
        context.globalCompositeOperation = "multiply";

        // Back wall
        context.fillStyle = this.colorSource.gradient(context, 0, 15, 0, 95, this.emptyProgress);
        context.fillRect(50, 15 + 80 * this.emptyProgress, this.width - 100, 80 * (1 - this.emptyProgress));

        // Left wall
        let slope = 1/9.4, fillHeight = 11;
        context.fillStyle = this.colorSource.gradient(context, 50 - fillHeight, 95 - fillHeight * slope, 50, 95, this.emptyProgress);
        context.beginPath();
        context.moveTo(50, 95);
        context.lineTo(25, this.height);
        context.lineTo(15, this.height);
        context.lineTo(50, 15);
        context.closePath();
        context.fill();

        // Right wall
        context.fillStyle = this.colorSource.gradient(context, this.width - 50 + fillHeight, 95 - fillHeight * slope, this.width - 50, 95, this.emptyProgress);
        context.beginPath();
        context.moveTo(this.width - 50, 95);
        context.lineTo(this.width - 25, this.height);
        context.lineTo(this.width - 15, this.height);
        context.lineTo(this.width - 50, 15);
        context.closePath();
        context.fill();

        // Floor
        context.fillStyle = this.colorSource.colorAt(this.emptyProgress);
        context.beginPath();
        context.moveTo(25, this.height);
        context.lineTo(50, 95);
        context.lineTo(this.width - 50, 95);
        context.lineTo(this.width - 25, this.height);
        context.closePath();
        context.fill();

        context.restore();
    }

    context.restore();
}
