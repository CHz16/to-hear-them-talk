// menulayer.js
// To hear them talk menu layer code.

let MENU_FONT = "32px sans-serif", MENU_FONT_SIZE = 32;
let MENU_HEADING_FONT = "bold 40px sans-serif", MENU_HEADING_FONT_SIZE = 40;
let MENU_SELECTED_ITEM_PREFIX = "\uFF65"; // halfwidth katakana middle dot
let MENU_PADDING = 10, MENU_LINE_HEIGHT = 40, MENU_HEADING_HEIGHT = 50;
let MENU_BOTTOM_ANTIPADDING = MENU_LINE_HEIGHT - MENU_FONT_SIZE; // this might not need to be a def

let MENUS = {
    top: {
        title: "◀︎▶︎ (To hear them talk)",
        options: [
            {
                text: "Start",
                action: function(key, option) {
                    MENUS["top"].options[0].text = "Resume";
                    MENUS["top"].options[1].disabled = false;
                    gameController.start(false);
                    return true;
                }
            },
            {
                text: "Restart",
                disabled: true,
                action: function(key, option) {
                    gameController.start(true);
                    this.currentMenuSelection = 0;
                }
            },
            {text: "Controls", destination: "controls"},
            {text: "Sound", destination: "sound"}
        ],
        cancelAction: 0
    },
    sound: {
        title: "Sound",
        options: [
            {
                text: "Sound",
                refreshText: function(option) { return "Volume: " + defaults.get("volume") + "%" },
                action: function(key, option) {
                    if (key == Key.action) {
                        return;
                    }
                    let delta = (key == Key.left) ? -10 : 10;
                    defaults.set("volume", Math.min(Math.max(0, defaults.get("volume") + delta), 100));
                    updateVolume();
                    return true;
                },
                arrowsPerformAction: true
            },
            {text: "Back", type: "back", destination: "top"}
        ],
        onEnter: function() {
            this.soundPlayer.play("playground", {loop: true});
        },
        onLeave: function() {
            this.soundPlayer.stop("playground");
        }
    },
    controls: {
        title: "Controls",
        options: [
            {
                text: "Movement",
                refreshText: function(option) { return defaults.get("tankControls") ? "Movement: Tank" : "Movement: Free"; },
                action: function(key, option) { defaults.set("tankControls", !defaults.get("tankControls")); return true; },
                arrowsPerformAction: true
            },
            {type: "space", width: 500},
            {
                text: "Up",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Up",
                key: Key.up
            },
            {
                text: "Down",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Down",
                key: Key.down
            },
            {
                text: "Left",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Left",
                key: Key.left
            },
            {
                text: "Right",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Right",
                key: Key.right
            },
            {
                text: "Select/Action",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Select/Action",
                key: Key.action
            },
            {
                text: "Back/Cancel",
                refreshText: "refreshRebindText",
                action: "rebindKey",
                name: "Back/Cancel",
                key: Key.cancel
            },
            {type: "space", width: 500},
            {text: "Back", type: "back", destination: "top"}
        ]
    },
    rebinding: {
        options: [
            {text: "Press the new key."},
            {text: "Press [escape] to cancel."}
        ]
    },
    resetcontrols: {
        options: [
            {text: "Press [escape] again to reset the controls to defaults."},
            {text: "Press any other key to cancel."}
        ]
    }
};

function MenuLayer() {
    this.invoke = function(func, args = []) {
        if (func === undefined) {
            return;
        } else if (typeof func == "string") {
            return this[func](...args);
        } else {
            return func.apply(this, args);
        }
    }

    this.drawMenu = function(context, menuIndex, selectedOption) {
        // Menu box
        let menu = MENUS[menuIndex];
        context.font = MENU_HEADING_FONT;
        let menuWidth = context.measureText(menu.title).width;
        context.font = MENU_FONT;
        for (let i = 0; i < menu.options.length; i++) {
            if (menu.options[i].width !== undefined) {
                menuWidth = Math.max(menuWidth, menu.options[i].width);
            } else if (menu.options[i].text !== undefined) {
                let metrics = context.measureText(MENU_SELECTED_ITEM_PREFIX + menu.options[i].text);
                menuWidth = Math.max(menuWidth, metrics.width);
            }
        }
        menuWidth = Math.ceil(menuWidth + 2 * MENU_PADDING);
        let menuHeight = menu.options.length * MENU_LINE_HEIGHT + 2 * MENU_PADDING + (menu.title !== undefined ? MENU_HEADING_HEIGHT : 0) - MENU_BOTTOM_ANTIPADDING;
        let menuX = (gameWidth - menuWidth) / 2;
        let menuY = (gameHeight - menuHeight) / 2;

        context.strokeStyle = "black";
        context.lineWidth = 2;
        context.save();
        context.shadowColor = "black";
        context.shadowBlur = 10;
        context.strokeRect(menuX, menuY, menuWidth, menuHeight);
        context.restore();
        context.fillStyle = "white";
        context.fillRect(menuX + 1, menuY + 1, menuWidth - 2, menuHeight - 2);

        // Heading text
        context.fillStyle = "black";
        if (menu.title !== undefined) {
            context.font = MENU_HEADING_FONT;
            drawText(context, menu.title, menuX + MENU_PADDING, menuY + MENU_PADDING, MENU_HEADING_FONT_SIZE);
            menuY += MENU_HEADING_HEIGHT;
        }

        // Menu text
        context.font = MENU_FONT;
        for (let i = 0; i < menu.options.length; i++) {
            if (menu.options[i].text !== undefined) {
                context.fillStyle = (menu.options[i].disabled !== true) ? "black" : "gray";
                let line = menu.options[i].text;
                if (i == selectedOption) {
                    line = MENU_SELECTED_ITEM_PREFIX + line;
                }
                drawText(context, line, menuX + MENU_PADDING, menuY + MENU_PADDING + i * MENU_LINE_HEIGHT, MENU_FONT_SIZE);
            }
        }
    }

    this.refreshMenuText = function() {
        for (key in MENUS) {
            for (let i = 0; i < MENUS[key].options.length; i++) {
                let newText = this.invoke(MENUS[key].options[i].refreshText, [MENUS[key].options[i]]);
                if (newText !== undefined) {
                    MENUS[key].options[i].text = newText;
                }
            }
        }
    }

    this.switchToMenu = function(option) {
        this.invoke(MENUS[this.currentMenu].onLeave);

        if (option.type != "back") {
            this.lastMenuSelections[option.destination] = 0;
        }
        this.currentMenu = option.destination;
        this.currentMenuSelection = this.lastMenuSelections[this.currentMenu];

        this.invoke(MENUS[this.currentMenu].onEnter);
    }

    // Menu callbacks
    this.rebindKey = function(key, option) {
        this.rebinding = true;
        MENUS["rebinding"].options[0].text = "Press the new key for " + option.name + ".";
        inputManager.rebind(option.key);
    }
    this.refreshRebindText = function(option) {
        return option.name + ": " + keyboardTranslator.niceNameOfKey(inputManager.nameForKey(option.key));
    }

    this.rebinding = undefined, this.resettingControls = undefined;

    this.currentMenu = undefined, this.currentMenuSelection = undefined, this.lastMenuSelections = undefined;

    this.soundPlayer = new SoundPlayer(soundBuffers, audioContext, masterVolumeNode);
}

// Game loop hooks
MenuLayer.prototype.update = function(timestamp, dt) {

}

MenuLayer.prototype.draw = function(context) {
    this.drawMenu(context, this.currentMenu, this.currentMenuSelection);

    if (this.rebinding) {
        this.drawMenu(context, "rebinding", -1);
    } else if (this.resettingControls) {
        this.drawMenu(context, "resetcontrols", -1);
    }
}

MenuLayer.prototype.pause = function(timestamp) {
    this.soundPlayer.pause();
}

MenuLayer.prototype.unpause = function(pauseTime) {
    this.soundPlayer.unpause();
}

MenuLayer.prototype.keyDown = function(key) {
    // Check if we're resetting controls and handle that specially
    if (this.resettingControls) {
        if (key == Key.escape) {
            inputManager.forceSetKeyBindings(defaultKeyBindings);
        }
        this.resettingControls = false;
        return;
    }

    let currentOption = MENUS[this.currentMenu].options[this.currentMenuSelection];
    if (key == Key.action || key == Key.cancel || key == Key.left || key == Key.right) {
        // Check if we should switch to another menu
        let switchOption;
        if (key == Key.action && currentOption.destination !== undefined) {
            switchOption = currentOption;
        } else if (key == Key.cancel && MENUS[this.currentMenu].cancelAction === undefined) {
            for (option of MENUS[this.currentMenu].options) {
                if (option.type == "back") {
                    switchOption = option;
                    break;
                }
            }
        }
        if (switchOption !== undefined) {
            this.switchToMenu(switchOption);
            return;
        }

        // Check if the current option performs an action
        let action;
        if ((key == Key.action || ((key == Key.left || key == Key.right) && currentOption.arrowsPerformAction === true)) && currentOption.action !== undefined) {
            action = currentOption.action;
        } else if (key == Key.cancel && MENUS[this.currentMenu].cancelAction !== undefined) {
            action = MENUS[this.currentMenu].options[MENUS[this.currentMenu].cancelAction].action;
        }
        if (this.invoke(action, [key, currentOption]) === true) {
            this.refreshMenuText();
        }
    } else if (key == Key.up || key == Key.down) {
        let delta = (key == Key.up) ? (MENUS[this.currentMenu].options.length - 1) : 1;
        do {
            this.currentMenuSelection = (this.currentMenuSelection + delta) % MENUS[this.currentMenu].options.length;
            currentOption = MENUS[this.currentMenu].options[this.currentMenuSelection];
        } while (currentOption.type == "space" || currentOption.disabled === true);
        this.lastMenuSelections[this.currentMenu] = this.currentMenuSelection;
    } else if (key == Key.escape) {
        if (this.currentMenu != "controls") {
            // If we're not on the controls menu, go to the controls menu
            this.switchToMenu({destination: "controls", direction: "left"});
        } else {
            // Else, show the reset controls message
            this.resettingControls = true;
        }
    }
}

MenuLayer.prototype.keyUp = function(key) {

}

// InputManager hooks
MenuLayer.prototype.keyRebound = function(currentBindings) {
    defaults.set("keyBindings", currentBindings);
    this.rebinding = false;
    this.refreshMenuText();
}

MenuLayer.prototype.rebindCanceled = function() {
    this.rebinding = false;
    this.refreshMenuText();
}

MenuLayer.prototype.rawKeyDown = function(event) {

}

MenuLayer.prototype.rawKeyUp = function(event) {

}

// Extra stuff
MenuLayer.prototype.resetMenus = function() {
    this.rebinding = false;
    this.resettingControls = false;

    this.currentMenu = "top";
    this.currentMenuSelection = 0;

    this.lastMenuSelections = {};
    for (key in MENUS) {
        this.lastMenuSelections[key] = 0;
    }
    this.refreshMenuText();
}
