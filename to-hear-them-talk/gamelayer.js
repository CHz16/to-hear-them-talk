// gamelayer.js
// To hear them talk game layer code.

let GamePhase = {
    startEndcapped: {allowMovement: true, wrapCamera: true, showTerminal: false},
    freeRoam: {allowMovement: true, wrapCamera: true, showTerminal: false},
    waitingForRoomAnimation: {allowMovement: false, wrapCamera: true, showTerminal: false},
    readingPlaque: {allowMovement: false, wrapCamera: true, showTerminal: false},
    waitingForFinaleEndcap: {allowMovement: true, wrapCamera: false, showTerminal: false},
    finaleEndcapped: {allowMovement: true, wrapCamera: false, showTerminal: false},
    usingTerminal: {allowMovement: false, wrapCamera: false, showTerminal: true},
    resetting: {allowMovement: false, wrapCamera: false, showTerminal: true},
    ending: {allowMovement: false, wrapCamera: false, showTerminal: true},
    credits: {allowMovement: false, wrapCamera: false, showTerminal: false}
};

let GAME_MAP_WIDTH = 5000; // pixels, also modified by game states
let GAME_CAMERA_DEAD_ZONE = 100; // pixels

let GAME_PLAQUE_FONT = "24px serif", GAME_PLAQUE_FONT_SIZE = 24, GAME_PLAQUE_LINE_HEIGHT = 30;

let GAME_SEGMENT_WIDTH = 1200, GAME_ROOM_START = 540, GAME_PLAQUE_START = 180, GAME_DOOR_START = 255;
let GAME_OBJECT_PROBE_MIN = 20, GAME_OBJECT_PROBE_MAX = 35, GAME_OBJECT_PROBE_STEP = 5;

let GAME_DRAIN_ANIMATION_DURATION = 500; // milliseconds
let GAME_DRAIN_ANIMATION_COLOR_PLAQUE_PROGRESS = 0.08; // when to color a plaque, from 0 to 1
let GAME_ENDGAME_SHUTDOWN_DURATION = 2000, GAME_ENDGAME_STARTUP_DURATION = 6000; // milliseconds


let COLOR_ALPHA_CROSSOVER = 0.25; // 0-1
function ColorSource(red, green, blue) {
    this.colorString = function(r, g, b) {
        return "rgb(" + r + "," + g + "," + b + ")";
    }
    this.alphaColorString = function(r, g, b, a) {
        return "rgba(" + r + "," + g + "," + b + "," + a + ")";
    }

    ColorSource.prototype.lowColor = function(progress) {
        let r = (2 - progress) * (this.red / 2), g = (2 - progress) * (this.green / 2), b = (2 - progress) * (this.blue / 2);
        return this.colorString(Math.floor(r), Math.floor(g), Math.floor(b));
    }

    this.red = red, this.green = green, this.blue = blue;
    this.color = this.colorString(red, green, blue);

    this.highAlphaColor = this.alphaColorString(red, green, blue, 0);
}
ColorSource.prototype.colorAt = function(progress) {
    progress = 1 - progress;

    if (progress <= 0) {
        return this.highAlphaColor;
    } else if (progress < COLOR_ALPHA_CROSSOVER) {
        progress = 1 - (COLOR_ALPHA_CROSSOVER - progress) / COLOR_ALPHA_CROSSOVER;
        return this.alphaColorString(this.red, this.green, this.blue, progress);
    } else if (progress == COLOR_ALPHA_CROSSOVER) {
        return this.color; // probably never called, lmao float comparison
    } else if (progress < 1) {
        progress = 1 - ((1 - COLOR_ALPHA_CROSSOVER) - (progress - COLOR_ALPHA_CROSSOVER)) / (1 - COLOR_ALPHA_CROSSOVER);
        return this.lowColor(progress);
    } else {
        return this.lowColor(1);
    }
}
ColorSource.prototype.gradient = function(context, x1, y1, x2, y2, progress) {
    let gradient = context.createLinearGradient(x1, y1, x2, y2);
    gradient.addColorStop(0, this.highAlphaColor);
    gradient.addColorStop(progress, this.highAlphaColor);

    if (progress < 1 - COLOR_ALPHA_CROSSOVER) {
        gradient.addColorStop(COLOR_ALPHA_CROSSOVER + progress, this.color);
    }

    gradient.addColorStop(1, this.colorAt(progress));
    return gradient;
}
let GAME_ROOM_COLOR_SOURCES = [new ColorSource(0, 192, 0), new ColorSource(255, 80, 0), new ColorSource(0, 192, 192), new ColorSource(128, 128, 128)];


function GameLayer() {
    this.drawSlice = function(context, mapLeft, sliceWidth, canvasX) {
        context.save();
        context.rect(canvasX, 0, sliceWidth, gameHeight);
        context.clip();

        let mapRight = mapLeft + sliceWidth;
        let drawHighlight = false;
        for (object of this.objects) {
            if (object.hidden === true) {
                continue;
            }
            if (object.x > mapRight || (object.x + object.width) < mapLeft) {
                continue;
            }
            object.draw(context, canvasX + object.x - mapLeft, object.y, this.t);

            drawHighlight |= (object == this.activeObject);
        }

        // Draw the highlight on top of all objects (so, the player mainly)
        if (drawHighlight) {
            let gray = Math.floor(90 + 40 * Math.sin(this.t / 150));
            context.strokeStyle = "rgb(" + gray + "," + gray + "," + gray + ")";
            context.lineWidth = 2;

            let pulseWidth = 6 + 1 * Math.sin(this.t / 100);
            context.strokeRect(canvasX + this.activeObject.x - mapLeft - pulseWidth, this.activeObject.y - pulseWidth, this.activeObject.width + pulseWidth * 2, this.activeObject.height + pulseWidth * 2);
        }
        context.restore();
    }


    this.t = undefined;
    this.cameraX = undefined;

    this.soundPlayer = new SoundPlayer(soundBuffers, audioContext, masterVolumeNode);
    this.soundPlayer.pause();

    this.openedRooms = undefined, this.roomOrder = undefined;
    this.savedDoorId = undefined, this.animationEndTime = undefined;

    this.parallaxLayers = [
        {amplitude: 100, a: 2000, b: 1900, width: 50, x: 0, y: 0, nudge: 0, nudgeFactor: 1.5, color: "black", alpha: 1, visibility: 0},
        {amplitude: 80, a: 1900, b: 1800, width: 40, x: 0, y: 0, nudge: 0, nudgeFactor: 1.8, color: "black", alpha: 0.8, visibility: 0},
        {amplitude: 60, a: 1800, b: 1700, width: 30, x: 0, y: 0, nudge: 0, nudgeFactor: 2.4, color: "black", alpha: 0.6, visibility: 0},
        {amplitude: 40, a: 1700, b: 1600, width: 20, x: 0, y: 0, nudge: 0, nudgeFactor: 3.2, color: "black", alpha: 0.4, visibility: 0}
    ];

    this.hallway = new Hallway();
    this.leftEndcap = new Endcap(), this.rightEndcap = new RightEndcap();

    this.gamePhase = undefined, this.savedGamePhase = undefined;
    this.player = new Player();
    this.activeObject = undefined;

    this.leftEndcapRequest = undefined, this.rightEndcapRequest = undefined;

    this.activeTerminalCircle = undefined, this.terminalData = undefined, this.startBox = undefined;
    this.terminalMenuOption = undefined;

    this.currentCredits = undefined, this.creditsThinking = undefined, this.creditsBankedTime = undefined, this.creditsComplete = undefined;

    this.rooms = [
        new Room(GAME_ROOM_COLOR_SOURCES[0], "playground", 4),
        new Room(GAME_ROOM_COLOR_SOURCES[1], "lecture", 3),
        new Room(GAME_ROOM_COLOR_SOURCES[2], "research", 3),
        new Room(GAME_ROOM_COLOR_SOURCES[3], "machine", 4)
    ];
    this.doors = [new Door(0), new Door(1), new Door(2), new Door(3)];

    this.plaques = [
        new Plaque(GAME_ROOM_COLOR_SOURCES[0].color, ["The tree the tempest with a crash of wood", "Throws down in front of us is not to bar", "Our passage to our journey's end for good,", "But just to ask us who we think we are"]),
        new Plaque(GAME_ROOM_COLOR_SOURCES[1].color, ["Insisting always on our own way so.", "She likes to halt us in our runner tracks,", "And make us get down in a foot of snow", "Debating what to do without an axe."]),
        new Plaque(GAME_ROOM_COLOR_SOURCES[2].color, ["And yet she knows obstruction is in vain:", "We will not be put off the final goal", "We have it hidden in us to attain,", "Not though we have to seize earth by the pole"]),
        new Plaque(GAME_ROOM_COLOR_SOURCES[3].color, ["And, tired of aimless circling in one place,", "Steer straight off after something into space."])
    ];

    this.leftTerminal = new LeftTerminal(), this.rightTerminal = new RightTerminal();

    this.objects = this.rooms.concat([this.hallway, this.leftEndcap, this.rightEndcap, this.leftTerminal, this.rightTerminal], this.doors, this.plaques, [this.player]); // sort by z-index
    // Preset all of these on object construction so we can just check .interactionHeight in the object interaction probe without caring about undefineds
    for (object of this.objects) {
        if (object.interactionHeight === undefined) {
            object.interactionHeight = object.height;
        }
    }
}

// Game loop hooks
GameLayer.prototype.update = function(timestamp, dt) {
    this.t += dt;

    // Early bailout
    // Defined in gamelayer-credits.js
    if (this.gamePhase == GamePhase.credits) {
        this.updateCredits(timestamp, dt);
        return;
    }


    //
    // MOVEMENT & CAMERA
    //

    if (this.gamePhase.allowMovement) {
        // If the camera has wrapped and the player is on the other side of the wrap, then we'll fudge the coordinates here just for math simplicity
        if (this.gamePhase.wrapCamera && this.player.x < this.cameraX) {
            this.player.x += GAME_MAP_WIDTH;
        }

        // Update the player's velocity and heading based on the directional inputs
        if (inputManager.keyIsDown(Key.right) || inputManager.keyIsDown(Key.up) || inputManager.keyIsDown(Key.down) || inputManager.keyIsDown(Key.left)) {
            if (defaults.get("tankControls")) {
                // Forwards & backwards
                if (inputManager.keyIsDown(Key.up)) {
                    this.player.velocity += dt / 2;
                } else if (inputManager.keyIsDown(Key.down)) {
                    this.player.velocity -= dt / 2;
                }

                // Turning
                if (inputManager.keyIsDown(Key.left)) {
                    this.player.heading += dt / 300;
                    this.player.velocity += dt / 5;
                } else if (inputManager.keyIsDown(Key.right)) {
                    this.player.heading -= dt / 300;
                    this.player.velocity += dt / 5;
                }
            } else {
                // Find what direction the player wants to go in
                let targetHeading = 0;
                if (inputManager.keyIsDown(Key.right) && inputManager.keyIsDown(Key.up)) {
                    targetHeading = Math.PI / 4;
                } else if (inputManager.keyIsDown(Key.up) && inputManager.keyIsDown(Key.left)) {
                    targetHeading = 3 * Math.PI / 4;
                } else if (inputManager.keyIsDown(Key.left) && inputManager.keyIsDown(Key.down)) {
                    targetHeading = 5 * Math.PI / 4;
                } else if (inputManager.keyIsDown(Key.down) && inputManager.keyIsDown(Key.right)) {
                    targetHeading = 7 * Math.PI / 4;
                } else if (inputManager.keyIsDown(Key.up)) {
                    targetHeading = Math.PI / 2;
                } else if (inputManager.keyIsDown(Key.left)) {
                    targetHeading = Math.PI;
                } else if (inputManager.keyIsDown(Key.down)) {
                    targetHeading = 3 * Math.PI / 2;
                }

                // Find which way to turn will get us there faster and then turn that way
                let counterClockwiseDistance = targetHeading - this.player.heading;
                if (counterClockwiseDistance < 0) {
                    counterClockwiseDistance += 2 * Math.PI;
                }
                if (counterClockwiseDistance < dt / 300 || counterClockwiseDistance > 2 * Math.PI - dt / 300) {
                    // If the distance to the target heading is less than the amount we'd rotate, then snap to the target heading instead of overshooting it
                    this.player.heading = targetHeading;
                } else if (counterClockwiseDistance < Math.PI) {
                    this.player.heading += dt / 300;
                } else {
                    this.player.heading -= dt / 300;
                }

                // Also move forward
                this.player.velocity += dt / 2;
            }
        } else {
            // Friction
            this.player.velocity = Math.max(0, this.player.velocity - dt / 2);
        }

        // Normalize the heading from 0-360˚
        this.player.heading = (this.player.heading + 2 * Math.PI) % (2 * Math.PI);

        // Speed limit
        if (Math.abs(this.player.velocity) > 150) {
            this.player.velocity = 150 * (this.player.velocity > 0 ? 1 : -1);
        }

        // Actually move the player
        this.player.x += (dt / 1000) * this.player.velocity * Math.cos(this.player.heading);
        this.player.y += (dt / 1000) * this.player.velocity * -Math.sin(this.player.heading);

        // Collision detection
        if (this.player.y < 480) {
            this.player.y = 480;
        } else if (this.player.y + this.player.height > 580) {
            this.player.y = 580 - this.player.height;
        }
        if (this.gamePhase == GamePhase.startEndcapped && this.player.x > GAME_MAP_WIDTH && this.player.x < GAME_MAP_WIDTH + 50) {
            this.player.x = GAME_MAP_WIDTH + 50;
        } else if (this.gamePhase == GamePhase.finaleEndcapped && this.player.x < this.hallway.x) {
            this.player.x = this.hallway.x;
        } else if (this.gamePhase == GamePhase.finaleEndcapped && this.player.x + this.player.width > this.hallway.x + this.hallway.width) {
            this.player.x = this.hallway.x + this.hallway.width - this.player.width;
        }

        // Save the camera position for in a sec
        let oldCameraX = this.cameraX;

        // Move the camera
        let cameraMiddle = this.cameraX + gameWidth / 2;
        if (this.player.x + this.player.width > cameraMiddle + GAME_CAMERA_DEAD_ZONE) {
            this.cameraX += this.player.x + this.player.width - cameraMiddle - GAME_CAMERA_DEAD_ZONE;
        } else if (this.player.x < cameraMiddle - GAME_CAMERA_DEAD_ZONE) {
            this.cameraX += this.player.x - cameraMiddle + GAME_CAMERA_DEAD_ZONE;
        }

        // See how far the camera moved horizontally and nudge the parallax layers
        for (parallaxLayer of this.parallaxLayers) {
            parallaxLayer.nudge = (parallaxLayer.width + parallaxLayer.nudge - (this.cameraX - oldCameraX) / parallaxLayer.nudgeFactor) % parallaxLayer.width;
        }

        // Wrap the player and camera
        if (this.gamePhase.wrapCamera) {
            this.player.x = this.player.x % GAME_MAP_WIDTH;
            if (this.player.x < 0) {
                this.player.x += GAME_MAP_WIDTH;
            }
            this.cameraX = this.cameraX % GAME_MAP_WIDTH;
            if (this.cameraX < 0) {
                this.cameraX += GAME_MAP_WIDTH;
            }
        }

        // Update the player position in the audio context, for panning
        this.soundPlayer.setPositionOfListener(this.player.x + this.player.width, this.player.y);

        // Check if the player is pointing at an interactive object
        this.activeObject = undefined;
        probe: for (let i = GAME_OBJECT_PROBE_MIN; i <= GAME_OBJECT_PROBE_MAX; i += GAME_OBJECT_PROBE_STEP) {
            let probeX = this.player.x + this.player.width / 2 + i * Math.cos(this.player.heading);
            let probeY = this.player.y + this.player.height / 2 - i * Math.sin(this.player.heading);

            for (object of this.objects) {
                if (object.interactive === true && probeX >= object.x && probeX <= object.x + object.width && probeY >= object.y && probeY <= object.y + object.interactionHeight) {
                    this.activeObject = object;
                    break probe;
                }
            }
        }
    }


    //
    // DRAIN ANIMATION
    //

    if (this.gamePhase == GamePhase.waitingForRoomAnimation) {
        if (this.t >= this.animationEndTime) {
            this.animationEndTime = undefined;

            this.hallway.colorSource = undefined;
            this.hallway.emptyProgress = undefined;
            this.hallway.doorLeft = undefined;

            this.rooms[this.savedDoorId].emptyProgress = 1;
            this.doors[this.savedDoorId].open = false;
            this.parallaxLayers[this.openedRooms].visibility = 1;

            this.soundPlayer.stop(this.rooms[this.savedDoorId].name);

            this.openedRooms += 1;
            if (this.openedRooms == 4) {
                this.gamePhase = GamePhase.waitingForFinaleEndcap;
                this.hallway.width += 500; // add a little extra for scrolling just in case(?)

                let room = this.rooms[this.savedDoorId];
                this.leftEndcapRequest = room.x - 400;
                this.rightEndcapRequest = room.x + room.width + 400;
                // We keep this.savedDoorId around for the map reshuffle later
            } else {
                this.gamePhase = GamePhase.freeRoam;
                this.savedDoorId = undefined;
            }
        } else {
            let progress = 1 - (this.animationEndTime - this.t) / GAME_DRAIN_ANIMATION_DURATION;
            this.hallway.emptyProgress = progress;
            this.rooms[this.savedDoorId].emptyProgress = progress;
            this.parallaxLayers[this.openedRooms].visibility = progress;

            if (progress >= GAME_DRAIN_ANIMATION_COLOR_PLAQUE_PROGRESS) {
                this.plaques[this.savedDoorId].isColored = true;
            }
        }
    }


    //
    // OTHER JUNK
    //

    // Move the parallax layers
    for (parallaxLayer of this.parallaxLayers) {
        parallaxLayer.x = parallaxLayer.nudge + parallaxLayer.amplitude * (1 + Math.sin(this.t / parallaxLayer.a));
        parallaxLayer.x %= parallaxLayer.width;
        parallaxLayer.y = parallaxLayer.amplitude * (1 + Math.sin(this.t / parallaxLayer.b));
        parallaxLayer.y %= parallaxLayer.width;
    }

    // Draw the terminal
    if (this.gamePhase.showTerminal) {
        this.updateTerminal(); // defined in gamelayer-terminal.js
    }


    //
    // GAME PHASE
    //

    // Phase change to start wrapping
    if (this.gamePhase == GamePhase.startEndcapped && this.cameraX < 500 && this.cameraX > this.leftEndcap.width) {
        // < 500 is a fudge for screen wrapping lmao
        this.gamePhase = GamePhase.freeRoam;
        GAME_MAP_WIDTH = GAME_SEGMENT_WIDTH * 4;
        this.leftEndcap.hidden = true;
        this.hallway.x = 0;
        this.hallway.width = GAME_MAP_WIDTH;
    }

    // Phase change to rebuild the map once we've seen an endcap
    if (this.gamePhase == GamePhase.waitingForFinaleEndcap && (this.cameraX < this.leftEndcapRequest || this.cameraX + gameWidth > this.rightEndcapRequest)) {
        this.gamePhase = GamePhase.finaleEndcapped;

        // Find where to place the left endcap and what the leftmost room will be, given which endcap we saw
        let leftEndcapRight, leftmostRoom, leftmostRoomX;
        if (this.cameraX < this.leftEndcapRequest) {
            leftEndcapRight = this.leftEndcapRequest;
            leftmostRoom = this.savedDoorId;
            leftmostRoomX = this.rooms[this.savedDoorId].x;
        } else {
            leftEndcapRight = this.leftEndcapRequest - 3 * GAME_SEGMENT_WIDTH;
            let i = this.roomOrder.indexOf(this.savedDoorId);
            leftmostRoom = this.roomOrder[(i + 1) % 4];
            leftmostRoomX = this.rooms[this.savedDoorId].x - 3 * GAME_SEGMENT_WIDTH;
        }

        this.hallway.width = 4950; // three room segments, plus room, plus two * 400 endcap lead-ins
        this.hallway.x = leftEndcapRight;

        this.leftEndcap.hidden = false;
        this.leftEndcap.x = leftEndcapRight - this.leftEndcap.width;
        this.rightEndcap.hidden = false;
        this.rightEndcap.x = this.hallway.x + this.hallway.width;

        this.leftTerminal.hidden = false;
        this.leftTerminal.x = this.leftEndcap.x + 25;
        this.rightTerminal.hidden = false;
        this.rightTerminal.x = this.rightEndcap.x + this.rightEndcap.width - 25 - this.rightTerminal.width;

        let start = this.roomOrder.indexOf(leftmostRoom);
        for (let i = 0; i < 4; i++) {
            let id = this.roomOrder[(start + i) % 4];
            let roomStart = leftmostRoomX + i * GAME_SEGMENT_WIDTH;
            this.rooms[id].x = roomStart;
            this.plaques[id].x = roomStart + GAME_PLAQUE_START;
            this.doors[id].x = roomStart + GAME_DOOR_START;
        }

        this.leftEndcapRequest = undefined, this.rightEndcapRequest = undefined;
        this.savedDoorId = undefined;
    }

    // Phase change to reset
    if (this.gamePhase == GamePhase.resetting && this.t >= this.animationEndTime) {
        this.initialize();
    }

    // Phase change to roll credits
    if (this.gamePhase == GamePhase.ending && this.t >= this.animationEndTime) {
        this.gamePhase = GamePhase.credits;
        this.animationEndTime = this.t + GAME_CREDITS_PAUSE_BEFORE_STARTING;
    }
}

GameLayer.prototype.draw = function(context) {
    // Defined in gamelayer-credits.js
    if (this.gamePhase == GamePhase.credits) {
        this.drawCredits(context);
        return;
    }

    // Draw the parallax layers in the background
    // Draw them in reverse order so they overlap correctly
    context.save();
    context.lineWidth = 2;
    for (let i = this.parallaxLayers.length - 1; i >= 0; i--) {
        let parallaxLayer = this.parallaxLayers[i];
        if (parallaxLayer.visibility == 0) {
            continue;
        }

        context.globalAlpha = parallaxLayer.visibility * parallaxLayer.alpha;
        context.strokeStyle = parallaxLayer.color;
        let x = parallaxLayer.x, y = parallaxLayer.y;
        context.beginPath();
        while (x <= gameWidth) {
            context.moveTo(x, 0);
            context.lineTo(x, gameHeight);
            x += parallaxLayer.width;
        }
        while (y <= gameHeight) {
            context.moveTo(0, y);
            context.lineTo(gameWidth, y);
            y += parallaxLayer.width;
        }
        context.stroke();
    }
    context.restore();

    // Draw the map
    if (this.gamePhase.wrapCamera) {
        let sliceWidth = Math.min(this.cameraX + gameWidth, GAME_MAP_WIDTH) - this.cameraX;
        this.drawSlice(context, this.cameraX, sliceWidth, 0);
        if (sliceWidth < gameWidth) {
            // Overlap by one pixel to prevents seams
            this.drawSlice(context, 0, gameWidth - sliceWidth + 1, sliceWidth - 1);
        }
    } else {
        this.drawSlice(context, this.cameraX, gameWidth, 0);
    }

    // Super screen wrapping fudge to draw the whole player, if they're overlapping the wrap
    if (this.player.x + this.player.width > GAME_MAP_WIDTH) {
        this.player.draw(context, this.player.x - this.cameraX, this.player.y, this.t);
    }

    // Draw the plaque
    if (this.gamePhase == GamePhase.readingPlaque) {
        let message = this.activeObject.message;

        context.strokeStyle = "black";
        context.lineWidth = 3;
        context.save();
        context.shadowColor = "black";
        context.shadowBlur = 10;
        context.strokeRect(150, 200, gameWidth - 300, gameHeight - 400);
        context.restore();
        context.fillStyle = "white";
        context.fillRect(150, 200, gameWidth - 300, gameHeight - 400);

        if (this.activeObject.isColored) {
            context.fillStyle = this.activeObject.color;
            context.font = GAME_PLAQUE_FONT;
            let startY = (gameHeight - GAME_PLAQUE_LINE_HEIGHT * message.length) / 2;
            for (let i = 0; i < message.length; i++) {
                let metrics = context.measureText(message[i]);
                drawText(context, message[i], (gameWidth - metrics.width) / 2, startY + i * GAME_PLAQUE_LINE_HEIGHT, GAME_PLAQUE_FONT_SIZE);
            }
        }
    }

    /*
    // debug visualization for the interaction probe
    let playerCenterX = this.player.x + this.player.width / 2 - this.cameraX;
    if (playerCenterX < 0) {
        playerCenterX += GAME_MAP_WIDTH;
    }
    let playerCenterY = this.player.y + this.player.width / 2;
    context.fillStyle = "blue";
    for (let i = GAME_OBJECT_PROBE_MIN; i <= GAME_OBJECT_PROBE_MAX; i += GAME_OBJECT_PROBE_STEP) {
        context.beginPath();
        context.arc(playerCenterX + i * Math.cos(this.player.heading), playerCenterY - i * Math.sin(this.player.heading), 2, 0, 2 * Math.PI);
        context.fill();
    }
    */

    // Draw the terminal
    if (this.gamePhase.showTerminal) {
        this.drawTerminal(context); // defined in gamelayer-terminal.js
    }

    // Fade to white
    if (this.gamePhase == GamePhase.resetting) {
        context.save();
        context.globalAlpha = 1 - (this.animationEndTime - this.t) / GAME_ENDGAME_STARTUP_DURATION;
        context.fillStyle = "white";
        context.fillRect(0, 0, gameWidth, gameHeight);
        context.restore();
    }

    // Fade to black
    if (this.gamePhase == GamePhase.ending) {
        context.save();
        context.globalAlpha = 1 - (this.animationEndTime - this.t) / GAME_ENDGAME_SHUTDOWN_DURATION;
        context.fillStyle = "black";
        context.fillRect(0, 0, gameWidth, gameHeight);
        context.restore();
    }
}

GameLayer.prototype.pause = function(timestamp) {
    this.soundPlayer.pause();
}

GameLayer.prototype.unpause = function(pauseTime) {
    this.soundPlayer.unpause();
}

// InputManager hooks
GameLayer.prototype.keyDown = function(key) {
    // Stop reading a plaque
    if (this.gamePhase == GamePhase.readingPlaque && (key == Key.action || key == Key.cancel)) {
        this.gamePhase = this.savedGamePhase;
        this.savedGamePhase = undefined;
        return;
    }

    // Terminal key actions
    if (this.gamePhase == GamePhase.usingTerminal) {
        if (key == Key.cancel) {
            // Stop using the terminal
            this.gamePhase = GamePhase.finaleEndcapped;
        } else if (key == Key.up || key == Key.down) {
            // Change the selected option
            this.terminalMenuOption = 1 - this.terminalMenuOption;
        } else if (key == Key.action) {
            this.animationEndTime = this.t + ((this.terminalMenuOption == 0) ? GAME_ENDGAME_STARTUP_DURATION : GAME_ENDGAME_SHUTDOWN_DURATION);
            this.gamePhase = (this.terminalMenuOption == 0) ? GamePhase.resetting : GamePhase.ending;
            this.soundPlayer.play((this.terminalMenuOption == 0) ? "startup" : "shutdown");
        }
        return;
    }

    // Perform the object's action
    if (this.gamePhase.allowMovement && key == Key.action && this.activeObject !== undefined) {
        this[this.activeObject.action]();
    }
}

GameLayer.prototype.keyUp = function(key) {

}

GameLayer.prototype.keyRebound = function(currentBindings) {

}

GameLayer.prototype.rebindCanceled = function() {

}

// Game controller actions
GameLayer.prototype.initialize = function() {
    this.soundPlayer.stopAllSounds();

    this.t = 0;
    this.cameraX = 0;
    GAME_MAP_WIDTH = 5000; // a little extra here at the beginning so we don't see the wrap

    for (parallaxLayer of this.parallaxLayers) {
        parallaxLayer.visibility = 0;
    }

    this.hallway.hidden = false;
    this.hallway.x = 50;
    this.hallway.width = 800;
    this.hallway.colorSource = undefined;
    this.hallway.emptyProgress = undefined;
    this.hallway.doorLeft = undefined;

    this.leftEndcap.hidden = false;
    this.leftEndcap.x = 0;
    this.rightEndcap.hidden = true;
    this.leftTerminal.hidden = true;
    this.rightTerminal.hidden = true;

    this.gamePhase = GamePhase.startEndcapped, this.savedGamePhase = undefined;
    this.player.x = 50, this.player.y = 500;
    this.player.heading = 0, this.player.velocity = 0;
    this.activeObject = undefined;

    this.leftEndcapRequest = undefined, this.rightEndcapRequest = undefined;

    this.activeTerminalCircle = undefined, this.terminalData = undefined, this.startBox = undefined;
    this.terminalMenuOption = undefined;

    this.currentCredits = undefined, this.creditsThinking = undefined, this.creditsBankedTime = undefined, this.creditsComplete = undefined;

    this.savedDoorId = undefined, this.animationEndTime = undefined;
    this.openedRooms = 0;
    this.roomOrder = [0, 1, 2, 3];
    // Fisher-Yates
    for (let i = 0; i < 3; i++) {
        let j = i + Math.floor(Math.random() * (4 - i));
        let temp = this.roomOrder[i];
        this.roomOrder[i] = this.roomOrder[j];
        this.roomOrder[j] = temp;
    }

    for (let i = 0; i < 4; i++) {
        this.rooms[this.roomOrder[i]].x = GAME_ROOM_START + i * GAME_SEGMENT_WIDTH;
        this.rooms[this.roomOrder[i]].emptyProgress = 0;

        this.doors[this.roomOrder[i]].x = GAME_ROOM_START + GAME_DOOR_START + i * GAME_SEGMENT_WIDTH;
        this.doors[this.roomOrder[i]].interactive = true;
        this.doors[this.roomOrder[i]].open = false;

        this.soundPlayer.play(this.rooms[this.roomOrder[i]].name, {
            x: this.doors[this.roomOrder[i]].x + this.doors[this.roomOrder[i]].width,
            y: this.doors[this.roomOrder[i]].y + this.doors[this.roomOrder[i]].height,
            loop: true
        });

        this.plaques[this.roomOrder[i]].isColored = false;
        this.plaques[this.roomOrder[i]].x = GAME_ROOM_START + GAME_PLAQUE_START + i * GAME_SEGMENT_WIDTH;
    }

    this.update(0, 0);
}

// Object hooks
GameLayer.prototype.readPlaque = function() {
    this.savedGamePhase = this.gamePhase;
    this.gamePhase = GamePhase.readingPlaque;
    this.player.velocity = 0;
}

GameLayer.prototype.openDoor = function() {
    this.gamePhase = GamePhase.waitingForRoomAnimation;
    this.player.velocity = 0;

    this.savedDoorId = this.activeObject.id;
    this.animationEndTime = this.t + GAME_DRAIN_ANIMATION_DURATION;

    this.hallway.colorSource = this.rooms[this.savedDoorId].colorSource;
    this.hallway.doorLeft = this.activeObject.x;

    this.parallaxLayers[this.openedRooms].color = this.rooms[this.savedDoorId].color;

    this.activeObject.open = true;
    this.activeObject.interactive = false;
    this.activeObject = undefined;

    this.soundPlayer.play("whoosh");
    this.soundPlayer.fadeOut(this.rooms[this.savedDoorId].name);
}

GameLayer.prototype.useTerminal = function() {
    this.gamePhase = GamePhase.usingTerminal;
    this.player.velocity = 0;

    this.activeTerminalCircle = undefined, this.terminalData = undefined, this.startBox = undefined;
    this.terminalMenuOption = 0;
}
