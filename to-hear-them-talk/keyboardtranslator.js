﻿// keyboardtranslator.js
// Object that knows "nicer"/platform-specific names of keys, for printing purposes
// InputManager lowercases all key names internally (because KeyboardEvent.key is case sensitive, i.e. "z" and "Z" are separate), so this also properly cases them again
// See this ref: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values

function KeyboardTranslator() {
    this.keyData = {
        // Modifier keys
        "alt": {mac: "Option (Alt)", default: "Alt"},
        "altgraph": "AltGr",
        "capslock": "Caps Lock",
        "control": "Control",
        "fn": "Fn",
        "fnlock": "F-Lock",
        "hyper": "Hyper",
        "meta": {mac: "Command (⌘)", windows: "Windows", chromebook: "Search", default: "Meta"},
        "numlock": "Num Lock",
        "scrolllock": "Scroll Lock",
        "shift": "Shift",
        "super": "Super",
        "symbol": "Symbol",
        "symbollock": "Symbol Lock",

        // Whitespace keys
        "enter": {mac: "Return / Enter", default: "Enter"},
        "tab": "Tab",
        " ": "Space", // same as below
        "spacebar": "Space", // same as above

        // Navigation keys
        "arrowdown": "Down Arrow", // same as below
        "down": "Down Arrow", // same as above
        "arrowleft": "Left Arrow", // same as below
        "left": "Left Arrow", // same as above
        "arrowright": "Right Arrow", // same as below
        "right": "Right Arrow", // same as above
        "arrowup": "Up Arrow", // same as below
        "up": "Up Arrow", // same as above
        "end": "End",
        "home": "Home",
        "pagedown": "Page Down",
        "pageup": "Page Up",

        // Editing keys
        "backspace": {mac: "Delete", default: "Backspace"},
        "clear": "Clear",
        "copy": "Copy",
        "crsel": "CrSel",
        "cut": "Cut",
        "delete": {mac: "Forward Delete", default: "Delete"},
        "eraseeof": "Erase EOF",
        "exsel": "ExSel",
        "insert": "Insert",
        "paste": "Paste",
        "redo": "Redo",
        "undo": "Undo",

        // UI keys
        "accept": "Accept",
        "again": "Again",
        "attn": "Attn",
        "cancel": "Cancel",
        "contextmenu": {windows: "Menu", default: "Context Menu"}, // same as below
        "apps": {windows: "Menu", default: "Context Menu"}, // same as above
        "escape": "Escape", // same as below
        "esc": "Escape", // same as above
        "execute": "Execute",
        "find": "Find",
        "finish": "Finish",
        "help": {mac: "Help/Insert", default: "Help"},
        "pause": "Pause Application",
        "play": "Play Application",
        "props": "Props",
        "select": "Select",
        "zoomin": "Zoom In",
        "zoomout": "Zoom Out",

        // Device keys
        "brightnessdown": "Brightness Down",
        "brightnessup": "Brightness Up",
        "eject": "Eject",
        "logoff": "Log Off",
        "power": "Power",
        "poweroff": "Power Off",
        "printscreen": "Print Screen",
        "hibernate": "Hibernate",
        "standby": "Standby",
        "wakeup": "Wake Up",

        // IME and composition keys
        "allcandidates": "All Candidates",
        "alphanumeric": "Alphanumeric",
        "codeinput": "Code Input",
        "compose": {x11: "Multi", default: "Compose"},
        "convert": "Convert (変換)", // Is it impossible to detect Japanese keyboard layouts specifically in JS?
        "dead": "Dead",
        "finalmode": "Final",
        "groupfirst": "First Group",
        "grouplast": "Last Group",
        "groupnext": "Next Group",
        "groupprevious": "Previous Group",
        "modechange": "Mode Change",
        "nextcandidate": "Next Candidate",
        "nonconvert": "NonConvert (無変換)", // Is it impossible to detect Japanese keyboard layouts specifically in JS?
        "previouscandidate": "Previous Candidate",
        "process": "Process",
        "singlecandidate": "Single Candidate",
        // Korean keyboards only
        "hangulmode": "한/영",
        "hanjamode": "한자",
        "junjamode": "Junja Mode", // 전자??? what even is this key
        // Japanese keyboards only
        "eisu": "英数",
        "hankaku": "半角", // same as below
        "halfwidth": "半角", // same as above
        "hiragana": "ひらがな",
        "hiraganakatakana": {mac: "かな", default: "カタカナ/ひらがな"},
        "kanamode": "Kana Mode", // カナ?
        "kanjimode": "Kanji Mode", // 漢字?
        "katakana": "カタカナ",
        "romaji": "ローマ字",
        "zenkaku": "全角", // same as below
        "fullwidth": "全角", // same as above
        "zenkakuhankaku": "半角/全角",

        // Function keys
        "f1": "F1",
        "f2": "F2",
        "f3": "F3",
        "f4": "F4",
        "f5": "F5",
        "f6": "F6",
        "f7": "F7",
        "f8": "F8",
        "f9": "F9",
        "f10": "F10",
        "f11": "F11",
        "f12": "F12",
        "f13": {mac: "Print Screen", default: "F13"},
        "f14": {mac: "Scroll Lock", default: "F14"},
        "f15": {mac: "Pause", default: "F15"},
        "f16": "F16",
        "f17": "F17",
        "f18": "F18",
        "f19": "F19",
        "f20": "F20",
        "soft1": "F21", // lmao is this bad
        "soft2": "F22",
        "soft3": "F23",
        "soft4": "F24",

        // Phone keys
        "appswitch": "App Switch",
        "call": "Call",
        "camera": "Camera",
        "camerafocus": "Camera Focus",
        "endcall": "End Call",
        "goback": "Phone - Back",
        "gohome": "Phone - Home", // same as below
        "mozhomescreen": "Phone - Home", // same as above
        "headsethook": "Headset Button",
        "lastnumberredial": "Redial",
        "notification": "Notification",
        "mannermode": "Notification Mode",
        "voicedial": "Voice Dial",

        // Multimedia keys
        "channeldown": "Channel Down",
        "channelup": "Channel Up",
        "mediafastforward": "Fast Forward", // same as below
        "fastfwd": "Fast Forward", // same as above
        "mediapause": "Pause", // "Some older applications use simply 'Pause'", conflicts with the same code in UI keys
        "mediaplay": "Play",
        "mediaplaypause": "Pause/Play",
        "mediarecord": "Record",
        "mediarewind": "Rewind",
        "mediastop": "Stop",
        "mediatracknext": "Next Track", // same as below
        "medianexttrack": "Next Track", // same as above
        "mediatrackprevious": "Previous Track", // same as below
        "mediaprevioustrack": "Previous Track", // same as above

        // Audio control keys
        "audiobalanceleft": "Balance Left",
        "audiobalanceright": "Balance Right",
        "audiobassdown": "Bass Down",
        "audiobassboostdown": "Bass Boost Down",
        "audiobassboosttoggle": "Bass Boost On/Off",
        "audiobassboostup": "Bass Boost Up",
        "audiobassup": "Bass Up",
        "audiofaderfront": "Fader Front",
        "audiofaderrear": "Fader Rear",
        "audiosurroundmodenext": "Next Surround Mode",
        "audiotrebledown": "Treble Down",
        "audiotrebleup": "Treble Up",
        "audiovolumedown": "Volume Down", // same as below
        "volumedown": "Volume Down", // same as above
        "audiovolumemute": "Mute/Unmute", // same as below
        "volumemute": "Mute/Unmute", // same as above
        "audiovolumeup": "Volume Up", // same as below
        "volumeup": "Volume Up", // same as above
        "microphonetoggle": "Microphone On/Off",
        "microphonevolumedown": "Microphone Volume Down",
        "microphonevolumemute": "Microphone Mute/Unmute",
        "microphonevolumeup": "Microphone Volume Up",

        // TV control keys
        "tv": "TV - TV Mode", // same as below
        "live": "TV - TV Mode", // same as above
        "tv3dmode": "TV - 3D On/Off",
        "tvantennacable": "TV - Antenna/Cable",
        "tvaudiodescription": "TV - Audio Description On/Off",
        "tvaudiodescriptionmixdown": "TV - Audio Description Mix Down",
        "tvaudiodescriptionmixup": "TV - Audio Description Mix Up",
        "tvcontentsmenu": "TV - Contents",
        "tvdataservice": "TV - Data Services",
        "tvinput": "TV - Change Input",
        "tvinputcomponent1": "TV - Component 1",
        "tvinputcomponent2": "TV - Component 2",
        "tvinputcomposite1": "TV - Composite 1",
        "tvinputcomposite2": "TV - Composite 2",
        "tvinputhdmi1": "TV - HDMI 1",
        "tvinputhdmi2": "TV - HDMI 2",
        "tvinputhdmi3": "TV - HDMI 3",
        "tvinputhdmi4": "TV - HDMI 4",
        "tvinputvga1": "TV - VGA 1",
        "tvmediacontext": "TV - Context Menu",
        "tvnetwork": "TV - Network On/Off",
        "tvnumberentry": "TV - Number Entry",
        "tvpower": "TV - Power",
        "tvradioservice": "TV - Radio",
        "tvsatellite": "TV - Satellite",
        "tvsatellitebs": "TV - Broadcast Satellite",
        "tvsatellitecs": "TV - Communication Satellite",
        "tvsatellitetoggle": "TV - Toggle Satellite",
        "tvterrestrialanalog": "TV - Analog Terrestrial",
        "tvterrestrialdigital": "TV - Digital Terrestrial",
        "tvtimer": "TV - Timer",

        // Media controller keys
        // there is literally no reason for me to keep adding these lmao
        "avrinput": "AV Receiver - Change Input",
        "avrpower": "AV Receiver - Power",
        "colorf0red": "Red Button",
        "colorf1green": "Green Button",
        "colorf2yellow": "Yellow Button",
        "colorf3blue": "Blue Button",
        "colorf4grey": "Grey Button",
        "colorf5brown": "Brown Button",
        "closedcaptiontoggle": "Closed Captions",
        "dimmer": "Dimmer",
        "displayswap": "Change Video Source",
        "dvr": "DVR",
        "exit": "Exit", // Also output instead of "Home" by Firefox 36 and earlier
        "favoriteclear0": "Clear Favorite 0",
        "favoriteclear1": "Clear Favorite 1",
        "favoriteclear2": "Clear Favorite 2",
        "favoriteclear3": "Clear Favorite 3",
        "favoriterecall0": "Recall Favorite 0",
        "favoriterecall1": "Recall Favorite 1",
        "favoriterecall2": "Recall Favorite 2",
        "favoriterecall3": "Recall Favorite 3",
        "favoritestore0": "Store Favorite 0",
        "favoritestore1": "Store Favorite 1",
        "favoritestore2": "Store Favorite 2",
        "favoritestore3": "Store Favorite 3",
        "guide": "Guide",
        "guidenextday": "Guide - Next Day",
        "guidepreviousday": "Guide - Previous Day",
        "info": "Info",
        "instantreplay": "Instant Replay",
        "link": "Link",
        "listprogram": "List Program",
        "livecontent": "Live Content",
        "lock": "Lock",
        "mediaapps": "Apps",
        "mediaaudiotrack": "Audio Track",
        "medialast": "Last",
        "mediaskipbackward": "Skip Backward",
        "mediaskipforward": "Skip Forward",
        "mediastepbackward": "Step Backward",
        "mediastepforward": "Step Forward",
        "mediatopmenu": "Top Menu",
        "navigatein": "Navigate In",
        "navigatenext": "Navigate Next",
        "navigateout": "Navigate Out",
        "navigateprevious": "Navigate Previous",
        "nextfavoritechannel": "Next Favorite",
        "nextuserprofile": "Next User Profile",
        "ondemand": "On Demand",
        "pairing": "Pair Device",
        "pinpdown": "Picture-in-Picture Down",
        "pinpmove": "Move Picture-in-Picture",
        "pinptoggle": "Picture-in-Picture",
        "pinpup": "Picture-in-Picture Up",
        "playspeeddown": "Play Speed Down",
        "playspeedreset": "Reset Play Speed",
        "playspeedup": "Play Speed Up",
        "randomtoggle": "Shuffle Mode",
        //"rclowbattery": "Low Battery", // not actually a key
        "recordspeednext": "Recording Speed",
        "rfbypass": "RF Bypass",
        "scanchannelstoggle": "Channel Scan",
        "screenmodenext": "Screen Mode",
        "settings": "Settings",
        "splitscreentoggle": "Split Screen",
        "stbinput": "Set-Top Box - Change Input",
        "stbpower": "Set-Top Box - Power",
        "subtitle": "Subtitles",
        "teletext": "Teletext",
        "videomodenext": "Video Mode",
        "wink": "Wink",
        "zoomtoggle": "Zoom Mode", // same as below
        "zoom": "Zoom Mode", // same as above

        // Speech recognition keys
        "speechcorrectionlist": "Speech Correction List",
        "speechinputtoggle": "Speech Input Mode",

        // Document keys
        "close": "Close",
        "new": "New",
        "open": "Open",
        "print": "Print",
        "save": "Save",
        "spellcheck": "Spell Check",
        "mailforward": "Mail - Forward",
        "mailreply": "Mail - Reply",
        "mailsend": "Mail - Send",

        // Application selector keys
        "launchcalculator": "Calculator",
        "launchcalendar": "Calendar",
        "launchcontacts": "Contacts",
        "launchmail": "Mail",
        "launchmediaplayer": "Media Player", // same as below 2
        "selectmedia": "Media Player", // same as above and below
        "mediaselect": "Media Player", // same as above 2
        "launchmusicplayer": "Music Player",
        "launchmycomputer": "My Computer",
        "launchphone": "Phone",
        "launchscreensaver": "Screen Saver",
        "launchspreadsheet": "Spreadsheet",
        "launchwebbrowser": "Web Browser",
        "launchwebcam": "Web Cam",
        "launchwordprocessor": "Word Processor",
        "launchapplication1": "Application 1", // bugged in Chrome https://bugs.chromium.org/p/chromium/issues/detail?id=612743
        "launchapplication2": "Application 2", // bugged in Chrome https://bugs.chromium.org/p/chromium/issues/detail?id=612743
        "launchapplication3": "Application 3",
        "launchapplication4": "Application 4",
        "launchapplication5": "Application 5",
        "launchapplication6": "Application 6",
        "launchapplication7": "Application 7",
        "launchapplication8": "Application 8",
        "launchapplication9": "Application 9",
        "launchapplication10": "Application 10",
        "launchapplication11": "Application 11",
        "launchapplication12": "Application 12",
        "launchapplication13": "Application 13",
        "launchapplication14": "Application 14",
        "launchapplication15": "Application 15",
        "launchapplication16": "Application 16",

        // Browser control keys
        "browserback": "Browser - Back",
        "browserfavorites": "Browser - Favorites",
        "browserforward": "Browser - Forward",
        "browserhome": "Browser - Home",
        "browserrefresh": "Browser - Refresh",
        "browsersearch": "Browser - Search",
        "browserstop": "Browser - Stop",

        // Numeric keypad keys
        // Deprecated values ("Decimal" etc.) are handled in InputManager
        "key11": "11",
        "key12": "12",
        // Clear is handled in Editing keys
        "separator": "Numpad Separator",

        // And then these
        "a": "A",
        "b": "B",
        "c": "C",
        "d": "D",
        "e": "E",
        "f": "F",
        "g": "G",
        "h": "H",
        "i": "I",
        "j": "J",
        "k": "K",
        "l": "L",
        "m": "M",
        "n": "N",
        "o": "O",
        "p": "P",
        "q": "Q",
        "r": "R",
        "s": "S",
        "t": "T",
        "u": "U",
        "v": "V",
        "w": "W",
        "x": "X",
        "y": "Y",
        "z": "Z"
    };

    this.categories = {};
}

KeyboardTranslator.prototype.niceNameOfKey = function(keyName) {
    let keyDatum = this.keyData[keyName];
    if (keyDatum === undefined) {
        return keyName;
    } else if (typeof keyDatum == "string") {
        return keyDatum;
    }

    for (category in keyDatum) {
        if (category == "default") {
            continue;
        }

        if (this.categories[category] === undefined) {
            if (category == "mac") {
                this.categories["mac"] = (window.navigator.platform.indexOf("Mac") !== -1);
            } else if (category == "windows") {
                this.categories["windows"] = (window.navigator.platform.indexOf("Win") !== -1);
            } else if (category == "chromebook") {
                this.categories["chromebook"] = (window.navigator.userAgent.indexOf("CrOS") !== -1);
            } else if (category == "firefox") {
                this.categories["firefox"] = (window.navigator.userAgent.indexOf("Firefox") !== -1);
            } else if (category == "x11") {
                this.categories["x11"] = (window.navigator.platform.indexOf("X11") !== -1);
            }
        }

        if (this.categories[category]) {
            return keyDatum[category];
        }
    }

    if (keyDatum.default !== undefined) {
        return keyDatum.default;
    } else {
        return errorString;
    }
}
