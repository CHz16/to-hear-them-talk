// gamelayer-credits.js
// GameLayer extension methods for drawing the credits, which I'm also separating out for some reason

let GAME_CREDITS_PAUSE_BEFORE_STARTING = 3000; // milliseconds
let GAME_CREDITS_PAUSE_WHEN_THINKING = 1000; // milliseconds
let GAME_CREDITS_CHARACTER_DURATION = 30; // milliseconds

let GAME_CREDITS = [
    "◀︎▶︎ (To hear them talk)",
    "",
    "A game-like substance by CHz",
    "Made for GAMES MADE QUICK??? III",
    "and PUBLIC DOMAIN 1923 REMIX JAM",
    "",
    "Text by Robert Frost",
    "Audio from Freesound: aharri6,",
    "artemis_ch, InspectorJ, joedeshon,",
    "metamorphmuses, Mxsmanic, & Timbre",
    "",
    "Thanks for playing!"
];

GameLayer.prototype.updateCredits = function(timestamp, dt) {
    if (this.creditsComplete) {
        return;
    }

    if (this.currentCredits === undefined && this.t >= this.animationEndTime) {
        this.creditsBankedTime = this.t - this.animationEndTime - dt;
        this.animationEndTime = undefined;
        this.currentCredits = [""];
        this.creditsThinking = true;
        this.creditsComplete = false;
    }

    this.creditsBankedTime += dt;
    while ((this.creditsThinking && this.creditsBankedTime > GAME_CREDITS_PAUSE_WHEN_THINKING) || (!this.creditsThinking && this.creditsBankedTime > GAME_CREDITS_CHARACTER_DURATION)) {
        if (this.creditsThinking) {
            this.creditsBankedTime -= GAME_CREDITS_PAUSE_WHEN_THINKING;
        } else {
            this.creditsBankedTime -= GAME_CREDITS_CHARACTER_DURATION;
        }

        let l = this.currentCredits.length - 1;
        this.creditsThinking = (GAME_CREDITS[l] === "");

        if (this.currentCredits[l].length == GAME_CREDITS[l].length) {
            if (l == GAME_CREDITS.length - 1) {
                this.creditsComplete = true;
            } else {
                this.currentCredits.push("");
            }
        } else {
            this.currentCredits[l] += GAME_CREDITS[l][this.currentCredits[l].length];
        }
    }
}

GameLayer.prototype.drawCredits = function(context) {
    context.fillStyle = "black";
    context.fillRect(0, 0, gameWidth, gameHeight);

    if (this.currentCredits === undefined) {
        return;
    }

    let x = 80, y = 80;
    context.fillStyle = "#30FF30";
    context.font = "30px monospace";
    for (let i = 0; i < this.currentCredits.length; i++) {
        drawText(context, this.currentCredits[i], x, y + 35 * i, 30);
    }

    // Cursor
    let lineMetrics = context.measureText(this.currentCredits[this.currentCredits.length - 1]);
    let characterMetrics = context.measureText(" ");
    context.fillRect(x + lineMetrics.width, y + 35 * (this.currentCredits.length - 1), characterMetrics.width, 30);
}
