◀︎▶︎ (To hear them talk)
======================

A tone piece for [GAMES MADE QUICK??? III](https://itch.io/jam/games-made-quick-iii) and [PUBLIC DOMAIN 1923 REMIX JAM](https://itch.io/jam/pd1923-remix-jam). Playable here: https://chz.itch.io/to-hear-them-talk

Everything here is by me and released under MIT (see LICENSE.txt), except:

The in-game text, which is from the Robert Frost poem "On a Tree Fallen Across the Road (To hear us talk)," published in *New Hampshire*

The `lecture` ambient noise, which is edited to loop from ["classroom_ambiance.wav" by joedeshon from Freesound](https://freesound.org/people/joedeshon/sounds/258094/) ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/))

The `machine` ambient noise, which is edited to loop from ["Ambience, Machine Factory, A.wav" by InspectorJ from Freesound](https://freesound.org/people/InspectorJ/sounds/385943/) ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))

The `playground` ambient noise, which is edited to loop from ["Playground noises in Luxembourg Gardens" by Mxsmanic on Freesound](https://freesound.org/people/Mxsmanic/sounds/162318/) ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/))

The `research` ambient noise, which is edited to loop from ["Library sounds" by artemis_ch on Freesound](https://freesound.org/people/artemis_ch/sounds/202044/)  ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))

The `shutdown` sound, which is ["110552_\_Noisehag_\_hard_drive_shut_down_01_remix.wav" by Timbre on Freesound](https://freesound.org/people/Timbre/sounds/112167/) ([CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/))

The `startup` sound, which is cropped from ["Grinder.wav" by aharri6 on Freesound](https://freesound.org/people/aharri6/sounds/71083/) ([Sampling Plus 1.0](https://creativecommons.org/licenses/sampling+/1.0/))

The `whoosh` sound, which is cropped and faded from ["1213 waveShaper.flac" by metamorphmuses on Freesound](https://freesound.org/people/metamorphmuses/sounds/62150/) ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))
